import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutComponent } from './shared/layout/layout.component';
import { LoginComponent } from './pages/login/login.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { PnotifyService } from './services/pnotify.service';
import { JwtInterceptor } from './utils/jwt-interceptor';
import { ErrorInterceptor } from './utils/error-interceptor';
import { ModuleComponent } from './pages/configuration/module/module.component';
// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts/ng2-charts';
// tslint:disable-next-line:max-line-length
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive'; // this includes the core NgIdleModule but includes keepalive providers for easy wireup

import { MomentModule } from 'angular2-moment';
import { PartiesComponent } from './pages/parties/parties/parties.component'; // optional, provides moment-style pipes for date formatting


// import ngx-translate and the http loader
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { ModuleListComponent } from './pages/configuration/module/module-list/module-list.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { PartyIdentificationComponent } from './shared/identification/identification.component';
import { BackSaveButtonComponent } from './shared/back-save-button/back-save-button.component';
import { RolesComponent } from './pages/configuration/roles/roles.component';
import { RolesListComponent } from './pages/configuration/roles/roles-list/roles-list.component';
import { ContactNumberComponent } from './shared/contact-number/contact-number.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';
import { VehiclesComponent } from './pages/vehicles/vehicles.component';
import { GeneralTypeService } from './services/general-type.service';
import { PropertiesComponent } from './pages/properties/properties.component';
import { CompanyComponent } from './pages/configuration/company/company.component';
import { CompanyListComponent } from './pages/configuration/company/company-list/company-list.component';
import { UserComponent } from './pages/configuration/user/user.component';
import { UsersListComponent } from './pages/configuration/user/users-list/users-list.component';
import { LoansRequestComponent } from './pages/loans-request/loans-request.component';
import { WarrantyComponent } from './pages/warranty/warranty.component';
import { ControlButtonsComponent } from './shared/control-buttons/control-buttons.component';
import { TgeneralComponent } from './pages/Configuration/tgeneral/tgeneral.component';
import { TgeneralListComponent } from './pages/Configuration/tgeneral/tgeneral-list/tgeneral-list.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { LocationComponent } from './shared/location/location.component';
import { BaseContentComponent } from './shared/base-content/base-content.component';
import { GenericCompComponent } from './shared/generic-comp/generic-comp.component';
import { BrandsComponent } from './pages/Generic/brands/brands.component';
import { DepartmentComponent } from './pages/Generic/department/department.component';
import { EmployeeComponent } from './pages/RRHH/employee/employee.component';
import { EmployeeListComponent } from './pages/RRHH/employee/employee-list/employee-list.component';
import { ContactPersonComponent } from './shared/contact-person/contact-person.component';
import { BanksComponent } from './pages/Accounting/banks/banks.component';
import { BanksListComponent } from './pages/Accounting/banks/banks-list/banks-list.component';
import { AccountTypesComponent } from './pages/Accounting/account-types/account-types.component';
import { BankAccountComponent } from './pages/Accounting/banks/bank-account/bank-account.component';
import { AccountGroupComponent } from './pages/Accounting/account-group/account-group.component';
import { AccountGroupListComponent } from './pages/Accounting/account-group/account-group-list/account-group-list.component';
import { AccountClassComponent } from './pages/Accounting/account-class/account-class.component';
import { AccountClassListComponent } from './pages/Accounting/account-class/account-class-list/account-class-list.component';
import { AccountRubroComponent } from './pages/Accounting/account-rubro/account-rubro.component';
import { AccountRubroListComponent } from './pages/Accounting/account-rubro/account-rubro-list/account-rubro-list.component';
import { AccountCatalogComponent } from './pages/Accounting/account-catalog/account-catalog.component';
import { AccountCatalogListComponent } from './pages/Accounting/account-catalog/account-catalog-list/account-catalog-list.component';
import { AccountPeriodComponent } from './pages/Accounting/account-period/account-period.component';
import { AccPeriodListComponent } from './pages/Accounting/account-period/acc-period-list/acc-period-list.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    LoginComponent,
    DashboardComponent,
    ModuleComponent,
    PartiesComponent,
    ModuleListComponent,
    PartyIdentificationComponent,
    BackSaveButtonComponent,
    RolesComponent,
    RolesListComponent,
    ContactNumberComponent,
    VehiclesComponent,
    PropertiesComponent,
    CompanyComponent,
    CompanyListComponent,
    UserComponent,
    UsersListComponent,
    LoansRequestComponent,
    WarrantyComponent,
    ControlButtonsComponent,
    TgeneralComponent,
    TgeneralListComponent,
    LocationComponent,
    BaseContentComponent,
    GenericCompComponent,
    BrandsComponent,
    DepartmentComponent,
    EmployeeComponent,
    EmployeeListComponent,
    ContactPersonComponent,
    BanksComponent,
    BanksListComponent,
    AccountTypesComponent,
    BankAccountComponent,
    AccountGroupComponent,
    AccountGroupListComponent,
    AccountClassComponent,
    AccountClassListComponent,
    AccountRubroComponent,
    AccountRubroListComponent,
    AccountCatalogComponent,
    AccountCatalogListComponent,
    AccountPeriodComponent,
    AccPeriodListComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NgxSpinnerModule,
    MomentModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    NgIdleKeepaliveModule.forRoot(),
    ModalModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  }),
  BsDatepickerModule.forRoot()
  ],
  providers: [PnotifyService, GeneralTypeService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }

// The HttpLoaderFactory is required for AOT (ahead of time) compilation in your project.
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
