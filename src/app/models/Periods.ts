export class Periods {
    GID: string;
    Indx: number;
    IsDeleted: boolean;
    IsActive: boolean;
    Status: string;
    LogDate: Date;
    PeriodId: string;
    Name: string
    PeriodState: string;
}