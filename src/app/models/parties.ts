import { ContactNumber } from './contact-number';
import { Location } from './location';
import { PartyIdentification } from './party-identification';

export class Parties {
  Selected: boolean;
  GID: string;
  Indx: number;
  IsDeleted: boolean;
  IsActive: boolean;
  Status: string;
  LogDate: Date;
  FirstName: string;
  LastName: string;
  FullName: string;
  Gender: string;
  MaritalStatus: string;
  BirthDate: Date;
  Email: string;

  ContactNumbers: Array<ContactNumber> = new Array<ContactNumber>();
  Identifications: Array<PartyIdentification> = new Array<PartyIdentification>();
  PartyLocations: Array<Location>;
  PersonsContact: Array<Parties>;
}
