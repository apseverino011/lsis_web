export class PeriodState {
    GID: string;
    Indx: number;
    IsDeleted: boolean;
    IsActive: boolean;
    Status: string;
    LogDate: Date;
    Code: string;
    Name: string
    Description: string;
}