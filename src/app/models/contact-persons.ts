import { Parties } from './parties';
import { Guid } from 'guid-typescript';

export class ContactPersons {
  GID: string;
  Indx: number;
  IsDeleted: boolean;
  IsActive: boolean;
  Status: string;
  LogDate: Date;
  OwnerId: string;
  ContactId: string;
  Number: string;
  Ext: string;
  IsMain: boolean;

  Contact: Parties;
}
