export class BaseResponse<T> {
  Success: boolean;
  Messages: {
    Message: string;
  };
  Data: T;
}
