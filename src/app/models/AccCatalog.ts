import { AccountGroup } from "./AccountGroup";
import { AccountRubro } from "./AccountRubro";
import { GeneralTypes } from "./general-types";
import { AccountClass } from "./AccClass";

export class AccountCatalog{
    GID: string;
    Indx: number;
    IsDeleted: boolean;
    IsActive: boolean;
    Status: string;
    LogDate: Date;
    AccFullNumber: string;
    AccNumber: string;
    AccName: string;
    AccGroupId: string;
    AccClassId: string;
    AccRubroId: string;
    AccOriginId: string;
    IsGeneralLedgerAcc: boolean;
    IsBalanceSheetAcc: boolean;
    AccBalanceSheetFunctionId: string;
    IsProfitAndLossAcc: boolean;
    IsCashFlowAcc: boolean;
    ParentAccountId: string;

    //Entitites
    AccGroup: AccountGroup;
    AccClass: AccountClass;
    AccRubro: AccountRubro;
    AccOrigin: GeneralTypes;
    ParentAccount: AccountCatalog;

    SubAccounts: Array<AccountCatalog> = new Array<AccountCatalog>();
}