export class PeriodDto {
    PeriodId: string;
    PeriodName: string;
    StartDate: string;
    EndDate: string;
    PeriodState: string;
    RegisterDate: Date;
}