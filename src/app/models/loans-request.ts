import { Guid } from 'guid-typescript';
import { Warranty } from './warranty';
import { Parties } from './parties';

export class LoansRequest {
  GID: Guid;
  Indx: number;
  IsDeleted: boolean;
  IsActive: boolean;
  Status: Guid;
  LogDate: Date;
  CompanyGID?: Guid;
  CreateBy: Guid;
  TypeGID: Guid;
  Number: string;
  PartyGID: Guid;
  Amount: number;
  Term: number;
  TermTypeGID: Guid;
  ApprovedBy?: Guid;
  ApprovedDate?: Date;
  Comment: string;
  Party: Parties = new Parties();
  Warranty: Warranty = new Warranty();
  Warranties: Array<Warranty> = new Array<Warranty>();
}
