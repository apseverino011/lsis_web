export class PartyIdentification {
  GID: string;
  Indx: number;
  IsDeleted: boolean;
  IsActive: boolean;
  Status: string;
  LogDate: Date;
  Party: string;
  IdentificationType: string;
  IdentificationTypeName: string;
  Number: string;
  SubSectorGID?: string;
  ExpirationDate: string;
}
