export class ParentAccount {
    AccountId: string;
    FullNumber: string;
    AccName: string;
    LogDate: Date;
}