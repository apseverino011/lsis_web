import { Periods } from "./Periods";

export class AccountPeriod {
    GID: string;
    Indx: number;
    IsDeleted: boolean;
    IsActive: boolean;
    Status: string;
    LogDate: Date;
    StartDate: string;
    EndDate: string;
    Name: string;
    PeriodState: string;

    PeriodDetails: Array<Periods> = new Array<Periods>();
}