import {AccountGroup} from './AccountGroup';

export class AccountRubro{
    GID: string;
    Indx: number;
    IsDeleted: boolean;
    IsActive: boolean;
    Status: string;
    LogDate: Date;
    AccRubroCode: string;
    AccRubroName: string;
    AccGroupGID: string;

    //Not Mapped
    AccGroup: AccountGroup = new AccountGroup();
    
}