export class BankAccount {
    GID: string;
    Indx: number;
    IsDeleted: boolean;
    IsActive: boolean;
    Status: string;
    LogDate: Date;
    BankId: string;
    AccountType: string;
    AccountTypeName: string;
    AccountNumber: string;

}