export class Location {
  GID: string;
  Indx: number;
  IsDeleted: boolean;
  IsActive: boolean;
  Status: string;
  LogDate: Date;
  PartyGID: string;
  SubSectorGID?: string;
  Adderss: string;
  StreetNumber?: number;
  ZipCode: string;
  Latitude: string;
  Longitude: string;
  IsMain: boolean;
}
