import {Parties} from '../models/parties';
import {UserRol} from '../models/Roles';

export class User {
    GID: string;
    Indx: number;
    IsDeleted: boolean;
    IsActive: boolean;
    Status: string;
    LogDate: Date;
    CompanyGID: string;
    PartyGID: string;
    FullName:string;
    UsrId: string;
    PssId: string;
    Language: string;
    UserRoleGID: string;
    ExpirationDate: Date;
    
    //Company employee properties
    Department: string;
    Position: string;
    Salary: number;

    //Entity person
    Party: Parties = new Parties();
    Role: UserRol = new UserRol();
    StatusName: string;
  }