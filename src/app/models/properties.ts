export class Properties {
  GID: string;
  Indx: number;
  IsDeleted: boolean;
  IsActive: boolean;
  Status: string;
  LogDate: Date;
  Type: string;
  Leves: number;
  Meters: number;
  Address: string;
  HasPool: boolean;
}
