import { Vehicles } from './vehicles';
import { Guid } from 'guid-typescript';
import { Parties } from './parties';
import { Properties } from './properties';

export class Warranty {
  GID: Guid;
  Indx: number;
  IsDeleted: boolean;
  IsActive: boolean;
  Status: Guid;
  LogDate: Date;
  RequestGID: Guid;
  WarrantyTypeGID: Guid;
  WarrantyGID: Guid;

  // Warranty according to it's type
  Person: Parties = new Parties();
  // This field is for type for Home, apartment and any other physical property
  Property: Properties = new Properties();
  Vehicle: Vehicles = new Vehicles();
}
