import { Parties } from "./parties";

export class Employee{
  GID: string;
  Indx: number;
  IsDeleted: boolean;
  IsActive: boolean;
  Status: string;
  LogDate: Date;
  CompanyGID: string;
  PartyGID: string;
  Department: string;
  Position: string;
  Salary: number;

  //Entity for relations
  Party: Parties;

}