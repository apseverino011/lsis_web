
export class Vehicles {
  GID: string;
  Indx: number;
  IsDeleted: boolean;
  IsActive: boolean;
  Status: string;
  LogDate: Date;
  BrandGID: string;
  ModelGID: string;
  Color: string;
  Year: number;
  LicensePlate: string;
  VIN: string;
  Enrollment: string;
}
