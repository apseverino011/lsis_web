import { AccountGroup } from "./AccountGroup";

export class AccountClass {
    GID: string;
    Indx: number;
    IsDeleted: boolean;
    IsActive: boolean;
    Status: string;
    LogDate: Date;
    AccClassCode: string;
    AccClassName: string;
    AccGroupId: string;

    //Not Mapped
    AccGroup: AccountGroup = new AccountGroup();;
}