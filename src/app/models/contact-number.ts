
export class ContactNumber {
  GID: string;
  Indx: number;
  IsDeleted: boolean;
  IsActive: boolean;
  Status: string;
  LogDate: Date;
  Party: string;
  ContactType: string;
  Number: string;
  Ext: string;
  IsMain: boolean;

  ContactTypeName: string
}
