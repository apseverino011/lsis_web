export class Login {
  Token: string;
  UserName: string;
  TimeSession: number;
  Modules: Modules;
}

export class Modules {
  Role: string;
  Name: string;
  Link: string;
  _select: boolean;
  _update: boolean;
  _delete: boolean;
  _insert: boolean;
  Options: Array<Options>;
}

export class Options {
  Name: string;
  Link: string;
  _select: boolean;
  _update: boolean;
  _delete: boolean;
  _insert: boolean;
  Options: Array<Options>;
}

export class NavData {
  name?: string;
  url?: string;
  icon?: string;
  badge?: any;
  title?: boolean;
  children?: any;
  variant?: string;
  attributes?: object;
  divider?: boolean;
  class?: string;
}
