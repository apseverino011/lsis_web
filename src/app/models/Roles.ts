import {GeneralModules} from '../models/GeneralModule';

export class UserRol {
    GID: string;
    Indx: number;
    IsDeleted: boolean;
    IsActive: boolean;
    Status: string;
    LogDate: Date;
    Name: string;
    Description: string;
    CompanyGID: string;

    Options: Array<RolesModulesOptions> = new Array<RolesModulesOptions>();

    EditOptions: Array<GeneralModules> = new Array<GeneralModules>();

}
export class RolesModulesOptions {
    GID: string;
    Indx: number;
    IsDeleted: boolean;
    IsActive: boolean;
    Status: string;
    LogDate: Date;
    UserRoleId: string;
    ModuleId: string;
    OptionId: string;
    IsMain: boolean;
    IsSub: boolean;
    _Select: boolean;
    _Update: boolean;
    _Delete: boolean;
    _Insert: boolean;

    Options: Array<RolesModulesOptions> = new Array<RolesModulesOptions>();
    Module: GeneralModules = new GeneralModules();
    Option: GeneralModules = new GeneralModules();

}