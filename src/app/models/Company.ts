import { ContactNumber } from "./contact-number";
import { Parties } from "./parties";
import { GeneralModules, Options } from "./GeneralModule";

export class Company{
    GID: string;
    Indx: number;
    IsDeleted: boolean;
    IsActive: boolean;
    Status: string;
    LogDate: Date;
    RNC: string;
    Code: string;
    BusinessName: string;
    Description: string;
    Url: string;
    Small_Logo_Path: string;
    Med_Logo_Path: string;
    Large_Logo_Path: string;

    ContactNumbers: Array<ContactNumber> = new Array<ContactNumber>();
    ContactPersons: Array<Parties> = new Array<Parties>();
    Locations: Array<Location> = new Array<Location>();
    Modules: Array<GeneralModules> = new Array<GeneralModules>();
}