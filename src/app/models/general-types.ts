export class GeneralTypes {
    Code: string;
    Description: string;
    GID: string;
    Indx: number;
    Status: string;
    OwnerId: string;
    StatusName: string;
    IsParent: boolean;
    IsActive: boolean;
    IsDeleted: boolean;

    Chields: Array<GeneralTypes> = new Array<GeneralTypes>();
}
