import { GeneralTypes } from "./general-types";

export class AccountGroup {
    GID: string;
    Indx: number;
    IsDeleted: boolean;
    IsActive: boolean;
    Status: string;
    LogDate: Date;
    AccOriginId: string;
    AccGroupCode: string;
    AccGroupName: string;
    AccTypeId: string;
    NextAccNumber: string;
    NextSubAccNumber: string;

    //Not Mapped
    AccountTypeName: string;
    AccountOriginName: string;
}