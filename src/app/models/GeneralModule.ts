import { Guid } from "guid-typescript";

export class Options {
    Select: boolean;
    GID: string;
    Indx: number;
    IsDeleted: boolean;
    IsActive: boolean;
    Status: string;
    LogDate: Date;
    Name: string
    Link: string
    IsMain: boolean
    IsSubM: boolean
    Owner: string
    Options: Array<Options> = new Array<Options>();
}

export class GeneralModules {
    Select: boolean;
    GID: string;
    Indx: number;
    IsDeleted: boolean;
    IsActive: boolean;
    Status: string;
    LogDate: Date;
    Name: string
    Link: string
    IsMain: boolean
    IsSubM: boolean
    Owner: string
    Options: Array<Options> = new Array<Options>();
}