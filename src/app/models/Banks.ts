import { BankAccount } from "./BankAccounts";

export class Bank {
    GID: string;
    Indx: number;
    IsDeleted: boolean;
    IsActive: boolean;
    Status: string;
    LogDate: Date;
    RNC: string;
    Name: string;
    Description: string;


    Accounts: Array<BankAccount> = new Array<BankAccount>();
}