import { Component, OnInit, Input } from '@angular/core';
import { Location } from 'src/app/models/location';
import { GeneralTypeService } from 'src/app/services/general-type.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { GeneralTypes } from 'src/app/models/general-types';
import { Guid } from 'guid-typescript';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.css']
})
export class LocationComponent implements OnInit {

  @Input() locations: Array<Location> = new Array<Location>();
  location: Location = new Location();
  locationForm: FormGroup;
  Sectors: Array<GeneralTypes> = new Array<GeneralTypes>();
  saveMode = true;
  constructor(private generalService: GeneralTypeService, formBuilder: FormBuilder) {
    this.locationForm = formBuilder.group({
      sector: ['', Validators.required],
      address: ['', Validators.required],
      streetNumber: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.getSectors();
  }

  getSectors() {
    this.generalService.GetGeneralTypeByCode(environment.generalTypesCode.Sectors).subscribe(result => {
      if (result.Success) {
        this.Sectors = result.Data.Chields;
      }
    });
  }

  onSave() {
    this.location.GID = Guid.create().toString();
    this.locations.push(this.location);
    this.location = new Location();
  }

  changeDate(object: Date) {
    if (object) {
      console.log(object.getDate());
    }
  }

  editlocation(location) {
    this.location = Object.assign({}, location);
    this.saveMode = false;
  }

  deletelocation(location: Location) {
    const index = this.locations.indexOf(location);
    this.locations.splice(index, 1);
  }

  onBack() {
    this.location = new Location();
  }

  onEdit() {
    // if (this.validateContactNumber()) { return; }

    const objIndex = this.locations.findIndex(x => x.GID == this.location.GID);

    this.locations[objIndex] = Object.assign({}, this.location);

    this.onBack();
  }

}
