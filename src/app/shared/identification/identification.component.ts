import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, Input } from '@angular/core';
import { PartyIdentification } from 'src/app/models/party-identification';
import { GeneralTypeService } from 'src/app/services/general-type.service';
import { GeneralTypes } from 'src/app/models/general-types';
import { Guid } from 'guid-typescript';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';
declare var $: any;
@Component({
  selector: 'app-party-identification',
  templateUrl: './identification.component.html',
  styleUrls: ['./identification.component.css']
})
export class PartyIdentificationComponent implements OnInit {

  @Input() identifications: Array<PartyIdentification> = new Array<PartyIdentification>();
  identification: PartyIdentification = new PartyIdentification();
  identificationTypes: Array<GeneralTypes> = new Array<GeneralTypes>();
  identificationForm: FormGroup;
  saveMode = true;
  constructor(private translate: TranslateService, private generalService: GeneralTypeService, formBuilder: FormBuilder) {
    this.identificationForm = formBuilder.group({
      type: ['', Validators.required],
      number: ['', Validators.required],
      enddate: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.getIdentificationTypes();
  }

  getIdentificationTypes() {
    this.generalService.GetIdentificationTypes().subscribe(result => {
      if (result.Success) {
        this.identificationTypes = result.Data;
      }
    });
  }


  onSave() {
    this.identification.GID = Guid.create().toString();
    let ident = this.identificationTypes.filter(x=> x.GID == this.identification.IdentificationType);
    this.identification.IdentificationTypeName = ident[0].Description;
    this.identifications.push(this.identification);
    this.identification = new PartyIdentification();
  }

  changeDate(object: Date) {
    if (object) {
      console.log(object.getDate());
    }
  }

  editIdentification(identification) {
    this.identification = Object.assign({}, identification);
    this.saveMode = false;
  }

  deleteIdentification(identification: PartyIdentification) {
    const index = this.identifications.indexOf(identification);
    this.identifications.splice(index, 1);
  }

  onBack() {
    this.identification = new PartyIdentification();
  }

  onEdit() {
    // if (this.validateContactNumber()) { return; }

    const objIndex = this.identifications.findIndex(x => x.GID == this.identification.GID);

    this.identifications[objIndex] = Object.assign({}, this.identification);

    this.onBack();
  }
}
