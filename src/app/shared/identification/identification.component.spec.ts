import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartyIdentificationComponent } from './identification.component';

describe('PartyIdentificationComponent', () => {
  let component: PartyIdentificationComponent;
  let fixture: ComponentFixture<PartyIdentificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartyIdentificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartyIdentificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
