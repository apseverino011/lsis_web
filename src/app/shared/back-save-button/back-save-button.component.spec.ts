import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackSaveButtonComponent } from './back-save-button.component';

describe('BackSaveButtonComponent', () => {
  let component: BackSaveButtonComponent;
  let fixture: ComponentFixture<BackSaveButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackSaveButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackSaveButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
