import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-back-save-button',
  templateUrl: './back-save-button.component.html',
  styleUrls: ['./back-save-button.component.css']
})
export class BackSaveButtonComponent implements OnInit {
  @Input() routerBack = '';
  @Input() showBackButtom = true;
  @Input() disabledSaveButtom = false;
  @Input() isSaveMode = true;
  @Input() textSaveButtom = 'Guardar';
  @Input() textEditButtom = 'Editar';
  @Input() textBackButtom = 'Volver';

  @Output() save: EventEmitter<any> = new EventEmitter();
  @Output() edit: EventEmitter<any> = new EventEmitter();
  @Output() back: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
    if (this.routerBack) {
      this.showBackButtom = false;
    }
  }

  guardar() {
    this.save.emit();
  }

  editar() {
    this.edit.emit();
  }

  volver() {
    this.back.emit();
  }

}
