import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenericCompComponent } from './generic-comp.component';

describe('GenericCompComponent', () => {
  let component: GenericCompComponent;
  let fixture: ComponentFixture<GenericCompComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenericCompComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenericCompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
