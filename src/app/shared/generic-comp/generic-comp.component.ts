import { Component, OnInit, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { GeneralTypes } from 'src/app/models/general-types';
import { GeneralTypeService } from 'src/app/services/general-type.service';
import { PnotifyService } from 'src/app/services/pnotify.service';
import { stringLiteral, thisExpression } from 'babel-types';
import { generate } from 'rxjs';
import { Guid } from 'guid-typescript';

declare const $: any;

@Component({
  selector: 'app-generic-comp',
  templateUrl: './generic-comp.component.html',
  styleUrls: ['./generic-comp.component.css']
})
export class GenericCompComponent implements OnInit {

  @Input() parentCode: string;
  generalType: GeneralTypes = new GeneralTypes();
  generalTypeClone: GeneralTypes = new GeneralTypes();
  chield: GeneralTypes = new GeneralTypes();
  subChield: GeneralTypes = new GeneralTypes();
  chieldClone: GeneralTypes = new GeneralTypes();
  subCh: boolean;
  subChields: Array<GeneralTypes> = new Array<GeneralTypes>();

  constructor(private generalServices: GeneralTypeService, private pnotifyService: PnotifyService,
    private translateService: TranslateService) {
    this.translateService.use('genericComp-es');
    //Temporal fix
    //this.parentCode = "VEHB";
  }

  ngOnInit() {
    if (this.parentCode != undefined && this.parentCode != "") {
      this.generalServices.GetGeneralTypeByCode(this.parentCode).subscribe(res => {
        if (res.Success) {
          this.generalType = res.Data;
        }
      });
    }
  }

  refresh() {
    if (this.parentCode != undefined && this.parentCode != "") {
      this.generalServices.GetGeneralTypeByCode(this.parentCode).subscribe(res => {
        if (res.Success) {
          this.generalType = res.Data;
          this.subCh = false;          
        }
      });
    }
  }

  save() {
    this.generalServices.Save(this.generalType).subscribe(res => {
      if (res.Success) {
        this.pnotifyService.showSuccess("The changed have been saved.");
        this.refresh();
      }
      else {
        this.pnotifyService.showError("The changed can't been saved.");
      }
    });
  }

  seeDetails(obj: GeneralTypes) {
    if (obj.Chields.length > 0) {
      this.subCh = true;
      this.chield = obj;
      this.subChields = obj.Chields;
    }
    else {
      this.subCh = false;
    }

  }

  removeChield(obj: GeneralTypes) {
    if (obj.Indx > 0) {
      obj.IsDeleted = true;
      obj.IsActive = false;
    }
    else {
      this.generalType.Chields.splice(this.generalType.Chields.indexOf(obj), 1);
    }
  }

  editChield(obj: GeneralTypes) {
    this.chieldClone = Object.assign({}, obj);
    this.chield = obj

    $("#detail").modal();
  }

  cancelEditChield() {
    let index = this.generalType.Chields.findIndex(x => x.GID == this.chield.GID);

    this.generalType.Chields[index] = this.chieldClone;

    $("#detail").modal('hide');
  }

  saveNewChield() {
    this.chield.Code = this.generalType.Code + "-" + this.chield.Code;

    if (this.validatechield()) { return; }

    this.generalType.Chields.push(this.chield);

    $("#detail").modal('hide');
  }

  addNewChield() {
    this.chield = new GeneralTypes();
    this.chield.GID = Guid.create().toString();
    this.chield.OwnerId = this.generalType.GID;

    $("#detail").modal();
  }

  addDetailsToChield(obj: GeneralTypes) {
    this.chield = obj;
    this.subChield = new GeneralTypes();
    this.subChield.GID = Guid.create().toString();
    this.subChield.OwnerId = obj.GID;

    $("#subDetail").modal();
  }

  saveNewSubChield() {
    let index = this.generalType.Chields.findIndex(x => x.GID == this.chield.GID);

    this.subChield.Code = this.chield.Code + "-" + this.subChield.Code;
    this.generalType.Chields[index].Chields.push(this.subChield);

    this.subChields = this.generalType.Chields[index].Chields;

    this.subCh = true;

    $("#subDetail").modal('hide');
  }

  validateGT() {

  }

  validatechield() {
    let chieldError = false;

    this.generalServices.validateCode(this.chield.Code).subscribe(res => {
      if (res.Data) {
        this.pnotifyService.showError("This code already exists");
        chieldError = true;
      }
    });

    return chieldError;
  }

  validateSubChield() {

  }

}
