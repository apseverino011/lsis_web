import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-base-content',
  templateUrl: './base-content.component.html',
  styleUrls: ['./base-content.component.css']
})
export class BaseContentComponent implements OnInit {

  @Input() saveButtonText: string;
  @Input() backButtonText: string;
  @Input() refreshButtonText: string;
  @Input() routerLink = '/';
  @Input() disableSaveButton = false;
  @Input() ActiveBack = true;
  @Input() ActiveSave = true;
  @Input() ActiveRefresh = true;

  @Output() save: EventEmitter<any> = new EventEmitter();
  @Output() back: EventEmitter<any> = new EventEmitter();
  @Output() refresh: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  onSave() {
    this.save.emit();
  }

  onBack() {
    this.back.emit();
  }

  onRefresh() {
    this.refresh.emit();
  }

}
