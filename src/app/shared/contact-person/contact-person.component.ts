import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Parties } from 'src/app/models/parties';
import { Guid } from 'guid-typescript';
import { PnotifyService } from '../../services/pnotify.service';
import { ContactPersons } from 'src/app/models/contact-persons';
import { GeneralTypeService } from 'src/app/services/general-type.service';
import { PartyService } from 'src/app/services/party.service';
import { GeneralTypes } from 'src/app/models/general-types';

declare const $: any;

class SearchValues {
  searchFor: string;
  value: string;
}

@Component({
  selector: 'app-contact-person',
  templateUrl: './contact-person.component.html',
  styleUrls: ['./contact-person.component.css']
})

export class ContactPersonComponent implements OnInit {
  @Input() contactPersons: Array<ContactPersons> = new Array<ContactPersons>();
  @Output() contactPersonsFormStatus: EventEmitter<any> = new EventEmitter<any>();

  @Input() isCompany: boolean;
  values: SearchValues = new SearchValues();
  partyClone: Parties = new Parties();
  party: Parties = new Parties();
  parties: Array<Parties> = new Array<Parties>();
  contactPerson: ContactPersons = new ContactPersons();
  filters: Array<GeneralTypes> = new Array<GeneralTypes>();

  constructor(private generalServices: GeneralTypeService, private partyServices: PartyService, private pnotifyService: PnotifyService) { }

  ngOnInit() {
    this.generalServices.GetDetailsByParentCode("IDT").subscribe(res => {
      if (res.Success) {
        this.filters = res.Data;
      }
    });
  }

  addNewCompany() {

  }

  search() {
    if (this.validateFilter()) { return; }

    let code = this.values.searchFor;
    switch (code) {
      case "EMP-COD":
        break;
      case "PAR-NAM":
        this.partyServices.FindPartyByName(this.values.value).subscribe(res => {
          if (res.Success) {
            this.parties = res.Data;
          }
        });
        break;
      case "IDT-ID":
      case "IDT-PASS":
        this.partyServices.FindPartyByIdent(this.values.value).subscribe(res => {
          if (res.Success) {
            this.parties = res.Data;
          }
        });
        break;
    }
  }

  searchContactPerson() {
    this.values = new SearchValues();
    this.parties = new Array<Parties>();
    $('#searchContactPerson').modal();
  }

  addNewContactPerson() {
    this.party = new Parties();
    this.party.GID = Guid.create().toString();

    $("#addNewContactPerson").modal();
  }

  saveNewContactPerson() {
    if (this.validateContact()) { return; }

    let index = this.contactPersons.findIndex(x => x.Contact.GID == this.party.GID);

    if (index == -1) {
      //Set Full Name
      this.party.FullName = this.party.FirstName + " " + this.party.LastName;

      //
      this.contactPerson = new ContactPersons();
      this.contactPerson.GID = Guid.create().toString();
      this.contactPerson.ContactId = this.party.GID;
      this.contactPerson.Contact = this.party;

      this.contactPersons.push(this.contactPerson);
    }

    $("#addNewContactPerson").modal('hide');
  }

  cancelAddContact() {
    let index = this.contactPersons.findIndex(x => x.Contact.GID == this.party.GID);
    if (index > -1) {
      this.contactPersons[index].Contact = this.partyClone;
      //this.contactPersons.splice(index, 1);
    }
    else {
      this.contactPerson = new ContactPersons();
      this.party = new Parties();
    }

    $("#addNewContactPerson").modal('hide');
  }

  viewDetails(cp: ContactPersons) {
    this.party = cp.Contact;

    this.partyClone = Object.assign({}, this.party);
    
    $("#addNewContactPerson").modal();
  }

  removeContactPerson(cn: ContactPersons) {
    let index = this.contactPersons.findIndex(x => x.GID == cn.GID);

    if (index != -1 && (cn.Indx == undefined || cn.Indx == 0)) {
      this.contactPersons.splice(index, 1);
    }
    else if (cn.Indx > 0) {
      cn.IsDeleted = true;
      cn.IsActive = false;
    }
  }

  addExists(par: Parties) {
    let index = this.contactPersons.findIndex(x => x.Contact.GID == par.GID);

    if (index == -1) {
      this.contactPerson = new ContactPersons();
      this.contactPerson.GID = Guid.create().toString();
      this.contactPerson.ContactId = par.GID;
      this.contactPerson.Contact = par;

      this.contactPersons.push(this.contactPerson);
    }
    else {
      this.contactPersons.splice(index, 1);
    }
  }

  save() {
    $("#searchContactPerson").modal('hide');
  }

  validateFilter() {
    let error = false;

    if (this.values.searchFor == undefined || this.values.searchFor == "00000000-0000-0000-0000-000000000000") {
      this.pnotifyService.showError("You need to select a criterion before the search");
      error = true;
    }

    if (this.values.value == undefined || this.values.value == "") {
      this.pnotifyService.showError("You need to specify a value to search");
      error = true;
    }

    return error;
  }

  validateContact() {
    let error = false;

    if (this.party.FirstName == undefined || this.party.FirstName.trim() == "") {
      this.pnotifyService.showError("The first name can't be empty");
      error = true;
    }

    if (this.party.LastName == undefined || this.party.LastName.trim() == "") {
      this.pnotifyService.showError("The last name can't be empty");
      error = true;
    }

    if (this.party.Email == undefined || this.party.Email.trim() == "") {
      this.pnotifyService.showError("The email is required");
      error = true;
    }

    if (this.party.ContactNumbers.length == 0) {
      this.pnotifyService.showError("At least a contact number has required");
      error = true;
    }

    if (this.party.Identifications.length == 0) {
      this.pnotifyService.showError("The identification is required");
      error = true;
    }

    return error;
  }

}
