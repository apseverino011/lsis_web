import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, Input, TemplateRef, Output, EventEmitter } from '@angular/core';
import { ContactNumber } from 'src/app/models/contact-number';
import { GeneralTypeService } from 'src/app/services/general-type.service';
import { GeneralTypes } from 'src/app/models/general-types';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Guid } from 'guid-typescript';
import { PnotifyService } from 'src/app/services/pnotify.service';
import { CustomValidationService } from 'src/app/services/custom-validation.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

declare var $: any;

@Component({
  selector: 'app-contact-number',
  templateUrl: './contact-number.component.html',
  styleUrls: ['./contact-number.component.css']
})
export class ContactNumberComponent implements OnInit {

  @Input() contactNumbers: Array<ContactNumber> = new Array<ContactNumber>();
  @Output() contactNumberFormStatus: EventEmitter<any> = new EventEmitter<any>();
  contactNumberForm: FormGroup;
  contactTypes: Array<GeneralTypes> = new Array<GeneralTypes>();
  contactNumber: ContactNumber = new ContactNumber();
  saveMode = true;

  constructor(private translateService: TranslateService,
    private generalService: GeneralTypeService,
    private formBuilder: FormBuilder,
    private pnotifyService: PnotifyService) {
    this.contactNumberForm = this.formBuilder.group({
      contactType: ['', Validators.required],
      number: ['', Validators.compose([Validators.required, Validators.minLength(3), CustomValidationService.numberValidator]) ],
      ext: [''],
      isMain: ['']
    });
  }

  ngOnInit() {
    this.getContactTypes();
    this.contactNumberForm.statusChanges.subscribe(data => {
      const status = data === 'VALID' ? true : false;
      this.contactNumberFormStatus.emit(status);
    });
  }

  onChange(type: any){
    //console.log(type);
    let gt = this.contactTypes.filter(x=> x.GID == type);

    this.contactNumber.ContactTypeName = gt[0].Description;
  }

  onSave() {
    if (this.validateContactNumber()) { return; }

    this.contactNumber.GID = Guid.create().toString();
    this.contactNumbers.push(this.contactNumber);
    this.contactNumber = new ContactNumber();
  }

  editContactNumber(contact) {
    this.contactNumber = Object.assign({}, contact);
    this.saveMode = false;
    // $('#party-contact-number-modal').modal('show');
  }

  deleteContactNumber(contact: ContactNumber) {
    contact.IsActive = false;
    contact.IsDeleted = true;
    /*const index = this.contactNumbers.indexOf(contact);
    this.contactNumbers.splice(index, 1);*/
  }

  validateContactNumber() {
    let response = false;

    if (this.contactNumbers.find(x => x.Number == this.contactNumber.Number && x.GID !== this.contactNumber.GID)) {
      response = true;
      this.pnotifyService.showError('Este numero de contacto ya existe');
    }

    return response;
  }

  getContactTypes() {
    this.generalService.GetPartyContactTypes().subscribe(result => {
      if (result.Success) {
        this.contactTypes = result.Data;
      }
    });
  }

  onBack() {
    this.contactNumber = new ContactNumber();
  }

  onEdit() {
    if (this.validateContactNumber()) { return; }

    const objIndex = this.contactNumbers.findIndex(x => x.GID == this.contactNumber.GID);

    this.contactNumbers[objIndex] = Object.assign({}, this.contactNumber);

    this.onBack();
  }

}
