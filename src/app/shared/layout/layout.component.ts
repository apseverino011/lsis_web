import { AuthenticationService } from './../../services/authentication.service';
import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import {Idle, DEFAULT_INTERRUPTSOURCES} from '@ng-idle/core';
import {Keepalive} from '@ng-idle/keepalive';
import { environment } from 'src/environments/environment';
import { NavData, Modules } from 'src/app/models/login';
import { DOCUMENT } from '@angular/common';

declare var $: any;

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css'],
  providers: [AuthenticationService]
})
export class LayoutComponent implements OnInit, OnDestroy {

  idleState = 'Not started.';
  timedOut = false;
  lastPing?: Date = null;
  currentUser = JSON.parse(localStorage.getItem(environment.keyLoginLocalStorage));
  modules: Array<Modules>;
  navItems: Array<NavData> = new Array<NavData>();
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement;

  constructor(private idle: Idle, private keepalive: Keepalive, private authService: AuthenticationService,
    @Inject(DOCUMENT) private _document?: any) {
    this.startAutomaticLogOut();
    this.generateModules();

    // This is temporary, when server is off
    // this.currentUser = {};
    // this.currentUser.UserName = 'Pedro';
    //console.log(this.currentUser);

  }

  ngOnDestroy(): void {
    this.changes.disconnect();
  }

  generateModules() {
    this.modules = this.currentUser.Modules;
    const item: NavData = new NavData();
    item.name = 'Modulos';
    item.title = true;
    this.navItems.push(item);
    this.modules.forEach(data => {
      const navItem = this.formarNav(data);
      this.navItems.push(navItem);
    });

    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = this._document.body.classList.contains('sidebar-minimized');
    });
    this.element = this._document.body;
    this.changes.observe(<Element>this.element, {
      attributes: true,
      attributeFilter: ['class']
    });

    // force temporal
    setTimeout(() => {
      $('.nav-dropdown').removeClass('open');
    }, 10);
  }

  formarNav(option: any) {
    const item: NavData = new NavData();
    item.name = option.Name;
    item.url = option.Link;
    item.icon = option.Options ? 'icon-puzzle' : 'icon-cursor' ;

    if (option.Options) {
      const children: Array<NavData> = new Array<NavData>();
      option.Options.forEach(subOption => {
        children.push(this.formarNav(subOption));
      });
      item.children = children;
    }
    // if (option.Options) {
    // } else {
    //   item.name = option.Name,
    //   item.url = option.Link,
    //   item.icon = 'icon-cursor'
    // }

    return item;
  }

  startAutomaticLogOut() {
    // console.log(this.currentUser);
    // sets an idle timeout of 5 seconds, for testing purposes.
    this.idle.setIdle(this.currentUser.TimeSession * 60);
    // sets a timeout period of 5 seconds. after 10 seconds of inactivity, the user will be considered timed out.
    this.idle.setTimeout(30);
    // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    this.idle.onIdleEnd.subscribe(() => this.idleState = 'Ya no está inactivo.');
    this.idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      this.authService.logout();
      // alert('cerrando sesion');
    });
    this.idle.onIdleStart.subscribe(() => {
      this.idleState = 'You\'ve gone idle!';
      $('#warningModal').modal('show');
      // document.getElementById('btnWarningModal').click();
    });
    this.idle.onTimeoutWarning.subscribe((countdown) => {
      this.idleState = 'Se cerrará la sesión en ' + countdown + ' segundos!';
      // document.getElementById('btnWarningModal').click();
    });

    // sets the ping interval to 15 seconds
    this.keepalive.interval(15);

    this.keepalive.onPing.subscribe(() => this.lastPing = new Date());

    this.reset();
  }

  reset() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }

  Logout() {
    this.authService.logout();
  }

  ngOnInit() {
  }

}
