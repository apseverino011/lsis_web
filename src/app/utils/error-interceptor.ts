import { NgxSpinnerService } from 'ngx-spinner';
import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthenticationService } from '../services/authentication.service';
import { PnotifyService } from '../services/pnotify.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    pnotify = undefined;
    constructor(private authenticationService: AuthenticationService, private spinner: NgxSpinnerService, pnotifyService: PnotifyService) {
      this.pnotify = pnotifyService.getPNotify();
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 401) {
                // auto logout if 401 response returned from api
                this.authenticationService.logout();
                location.reload(true);
            }
            const error = err.error.message || err.statusText;
            this.spinner.hide();
            this.pnotify.error('ha ocurrido un error desconocido');
            return throwError(error);
        }));

    }
}
