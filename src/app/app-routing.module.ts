import { PartiesComponent } from './pages/parties/parties/parties.component';
import { AuthGuard } from './utils/auth-guard';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ModuleComponent } from './pages/configuration/module/module.component';
import { ModuleListComponent } from './pages/configuration/module/module-list/module-list.component';
import { RolesComponent } from './pages/configuration/roles/roles.component';
import { RolesListComponent } from './pages/configuration/roles/roles-list/roles-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { LayoutComponent } from './shared/layout/layout.component';
import { CompanyComponent } from './pages/configuration/company/company.component';
import { CompanyListComponent } from './pages/configuration/company/company-list/company-list.component';
import { UserComponent } from './pages/configuration/user/user.component';
import { UsersListComponent } from './pages/configuration/user/users-list/users-list.component';
import { LoansRequestComponent } from './pages/loans-request/loans-request.component';
import { TgeneralComponent } from './pages/Configuration/tgeneral/tgeneral.component';
import { TgeneralListComponent } from './pages/Configuration/tgeneral/tgeneral-list/tgeneral-list.component';
import { GenericCompComponent } from 'src/app/shared/generic-comp/generic-comp.component';
import { BrandsComponent } from 'src/app/pages/Generic/brands/brands.component';
import { DepartmentComponent } from 'src/app/pages/Generic/department/department.component';
import { BanksComponent } from 'src/app/pages/Accounting/banks/banks.component';
import { BanksListComponent } from 'src/app/pages/Accounting/banks/banks-list/banks-list.component';
import { AccountTypesComponent } from 'src/app/pages/Accounting/account-types/account-types.component';
import { AccountGroupListComponent } from 'src/app/pages/Accounting/account-group/account-group-list/account-group-list.component';
import { AccountGroupComponent } from 'src/app/pages/Accounting/account-group/account-group.component';
import { AccountClassListComponent } from 'src/app/pages/Accounting/account-class/account-class-list/account-class-list.component';
import { AccountClassComponent } from 'src/app/pages/Accounting/account-class/account-class.component';
import { AccountRubroListComponent } from 'src/app/pages/Accounting/account-rubro/account-rubro-list/account-rubro-list.component';
import { AccountRubroComponent } from 'src/app/pages/Accounting/account-rubro/account-rubro.component';
import { AccountCatalogListComponent } from 'src/app/pages/Accounting/account-catalog/account-catalog-list/account-catalog-list.component';
import { AccountCatalogComponent } from 'src/app/pages/Accounting/account-catalog/account-catalog.component';
import { AccountPeriodComponent } from './pages/Accounting/account-period/account-period.component';
import { AccPeriodListComponent } from './pages/Accounting/account-period/acc-period-list/acc-period-list.component';

const routes: Routes = [
  // no layout routes
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: '', component: DashboardComponent, canActivate: [AuthGuard] },
      { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },

      // Module
      { path: 'module', component: ModuleListComponent, canActivate: [AuthGuard] },
      { path: 'module/new', component: ModuleComponent, canActivate: [AuthGuard] },
      { path: 'module/edit/:id', component: ModuleComponent, canActivate: [AuthGuard] },

      // Roles
      { path: 'roles', component: RolesListComponent, canActivate: [AuthGuard] },
      { path: 'role/new', component: RolesComponent, canActivate: [AuthGuard] },
      { path: 'role/edit/:id', component: RolesComponent, canActivate: [AuthGuard] },

      // parties
      { path: 'parties', component: PartiesComponent },

      // Companies
      { path: 'companies', component: CompanyListComponent, canActivate: [AuthGuard] },
      { path: 'company/new', component: CompanyComponent, canActivate: [AuthGuard] },
      { path: 'company/edit/:id', component: CompanyComponent, canActivate: [AuthGuard] },

      // Users
      { path: 'users', component: UsersListComponent, canActivate: [AuthGuard] },
      { path: 'user/new', component: UserComponent, canActivate: [AuthGuard] },
      { path: 'user/edit/:id', component: UserComponent, canActivate: [AuthGuard] },

      // Loans Request
      { path: 'loans-request', component: LoansRequestComponent, canActivate: [AuthGuard] },

      // General Types
      { path: 'gtypes', component: TgeneralListComponent, canActivate: [AuthGuard] },
      { path: 'gtypes/new', component: TgeneralComponent, canActivate: [AuthGuard] },
      { path: 'gtypes/edit/:id', component: TgeneralComponent, canActivate: [AuthGuard] },

      // Companies
      { path: 'gtypes', component: TgeneralListComponent, canActivate: [AuthGuard] },
      { path: 'gtypes/new', component: TgeneralComponent, canActivate: [AuthGuard] },
      { path: 'gtypes/edit/:id', component: TgeneralComponent, canActivate: [AuthGuard] },

      // Bank
      { path: 'banks', component: BanksListComponent, canActivate: [AuthGuard] },
      { path: 'bank/new', component: BanksComponent, canActivate: [AuthGuard] },
      { path: 'bank/edit/:id', component: BanksComponent, canActivate: [AuthGuard] },

      //================================= Accounting =============================================
      //Account Type
      { path: 'accType', component: AccountTypesComponent, canActivate: [AuthGuard] },

      //Account Group
      { path: 'accGroups', component: AccountGroupListComponent, canActivate: [AuthGuard] },
      { path: 'accGroup/new', component: AccountGroupComponent, canActivate: [AuthGuard] },
      { path: 'accGroup/edit/:id', component: AccountGroupComponent, canActivate: [AuthGuard] },
      
      //Account Group
      { path: 'accClasses', component: AccountClassListComponent, canActivate: [AuthGuard] },
      { path: 'accClass/new', component: AccountClassComponent, canActivate: [AuthGuard] },
      { path: 'accClass/edit/:id', component: AccountClassComponent, canActivate: [AuthGuard] },
      
      //Account Rubro
      { path: 'accRubros', component: AccountRubroListComponent, canActivate: [AuthGuard] },
      { path: 'accRubro/new', component: AccountRubroComponent, canActivate: [AuthGuard] },
      { path: 'accRubro/edit/:id', component: AccountRubroComponent, canActivate: [AuthGuard] },
      
      //Account Catalogs
      { path: 'accCatalogs', component: AccountCatalogListComponent, canActivate: [AuthGuard] },
      { path: 'accCatalog/new', component: AccountCatalogComponent, canActivate: [AuthGuard] },
      { path: 'accCatalog/edit/:id', component: AccountCatalogComponent, canActivate: [AuthGuard] },
      
      //Account Periods
      { path: 'accPeriods', component: AccPeriodListComponent, canActivate: [AuthGuard] },
      { path: 'accPeriod/new', component: AccountPeriodComponent, canActivate: [AuthGuard] },
      { path: 'accPeriod/edit/:id', component: AccountPeriodComponent, canActivate: [AuthGuard] },
      //==========================================================================================
      
      //Brands
      { path: 'brands', component: BrandsComponent, canActivate: [AuthGuard] },

      //Department
      { path: 'department', component: DepartmentComponent, canActivate: [AuthGuard] },

      //General type Test
      { path: 'type', component: GenericCompComponent, canActivate: [AuthGuard] },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
