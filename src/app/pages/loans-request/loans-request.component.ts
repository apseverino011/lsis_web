import { Component, OnInit } from '@angular/core';
import { LoansRequest } from 'src/app/models/loans-request';
import { TranslateService } from '@ngx-translate/core';
import { GeneralTypeService } from 'src/app/services/general-type.service';
import { GeneralTypes } from 'src/app/models/general-types';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidationService } from 'src/app/services/custom-validation.service';
import { Warranty } from 'src/app/models/warranty';
import { LoansRequestService } from 'src/app/services/loans-request.service';

@Component({
  selector: 'app-loans-request',
  templateUrl: './loans-request.component.html',
  styleUrls: ['./loans-request.component.css'],
})
export class LoansRequestComponent implements OnInit {
  request: LoansRequest = new LoansRequest();
  requestForm: FormGroup;
  personFormValid = false;
  warrantyFormValid = false;
  warrantiesTypes: GeneralTypes[];
  termTypes: GeneralTypes[];
  requestTypes: GeneralTypes[];
  constructor(
    private translateService: TranslateService,
    private generalService: GeneralTypeService,
    formBuilder: FormBuilder,
    private loansRequestService: LoansRequestService
  ) {
    this.translateService.use('loans-request-es');
    this.requestForm = formBuilder.group({
      type: ['', Validators.required],
      termType: ['', Validators.required],
      amount: ['', Validators.compose([Validators.required, CustomValidationService.numberValidator])],
      comment: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.getRequestTypes();
    this.getTermTypes();
  }

  getTermTypes() {
    this.generalService.GetTermTypes().subscribe(result => {
      if (result.Success) {
        this.termTypes = result.Data;
      }
    });
  }

  getRequestTypes() {
    this.generalService.GetRequestTypes().subscribe(result => {
      if (result.Success) {
        this.requestTypes = result.Data;
      }
    });
  }

  saveLoans() {
    this.request.Warranties = new Array<Warranty>();
    this.request.Warranties.push(this.request.Warranty);
    console.log(JSON.stringify(this.request));
    this.loansRequestService.CreateRequest(this.request).subscribe(result => {
      console.log('result', result);
    });
  }

  personFormStatusChange(status) {
    this.personFormValid = status;
  }

  warrantyFormStatusChange(status) {
    this.warrantyFormValid = status;
  }
}
