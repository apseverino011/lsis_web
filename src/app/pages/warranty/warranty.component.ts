import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Warranty } from 'src/app/models/warranty';
import { GeneralTypeService } from 'src/app/services/general-type.service';
import { GeneralTypes } from 'src/app/models/general-types';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-warranty',
  templateUrl: './warranty.component.html',
  styleUrls: ['./warranty.component.css']
})
export class WarrantyComponent implements OnInit {

  @Input() warranty: Warranty;
  @Output() warrantyFormStatus: EventEmitter<any> = new EventEmitter<any>();
  formStatus = false;
  warrantiesTypes: GeneralTypes[];
  enviroment = environment;
  warrantySelected = environment.Codes.WarrantyVehicle;
  constructor(private generalService: GeneralTypeService) { }

  ngOnInit() {
    this.getAllWarrantyTypes();
  }

  vehicleForm(data) {
    this.formStatus = data;
    this.warrantyFormStatus.emit(this.formStatus);
  }

  propertyForm(data) {
    this.formStatus = data;
    this.warrantyFormStatus.emit(this.formStatus);
  }

  personForm(data) {
    this.formStatus = data;
    this.warrantyFormStatus.emit(this.formStatus);
  }

  getAllWarrantyTypes() {
    this.generalService.GetWarrantyTypes().subscribe(result => {
      if (result.Success) {
        this.warrantiesTypes = result.Data;
      }
    });
  }
}
