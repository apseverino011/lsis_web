import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountRubroComponent } from './account-rubro.component';

describe('AccountRubroComponent', () => {
  let component: AccountRubroComponent;
  let fixture: ComponentFixture<AccountRubroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountRubroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountRubroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
