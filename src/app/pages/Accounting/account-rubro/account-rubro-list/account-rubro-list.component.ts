import { Component, OnInit } from '@angular/core';
import { AccountRubro } from 'src/app/models/AccountRubro';
import { AccountRubroService } from 'src/app/services/account-rubro.service';
import { Router } from '@angular/router';
import { PnotifyService } from 'src/app/services/pnotify.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-account-rubro-list',
  templateUrl: './account-rubro-list.component.html',
  styleUrls: ['./account-rubro-list.component.css']
})
export class AccountRubroListComponent implements OnInit {
  rubros: Array<AccountRubro> = new Array<AccountRubro>();
  constructor(private rubroServices: AccountRubroService, private router: Router
    , private pnitificationServices: PnotifyService
    , private translate: TranslateService) { 
      this.translate.setDefaultLang("rubro-es");
    }

  ngOnInit() {
    this.rubroServices.GetAll(true).subscribe(res => {
      if (res.Success) {
        this.rubros = res.Data;
      }
    });
  }

  edit(rubro: AccountRubro) {
    this.router.navigateByUrl("/accRubro/edit/" + rubro.GID);
  }

  remove(rubro: AccountRubro) {
    rubro.IsDeleted = true;
    rubro.IsActive = false;

    this.rubroServices.Save(rubro).subscribe(res => {
      if (res.Success) {
        this.pnitificationServices.showError("The rubro have been removed.");
      }
    });
  }

}
