import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountRubroListComponent } from './account-rubro-list.component';

describe('AccountRubroListComponent', () => {
  let component: AccountRubroListComponent;
  let fixture: ComponentFixture<AccountRubroListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountRubroListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountRubroListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
