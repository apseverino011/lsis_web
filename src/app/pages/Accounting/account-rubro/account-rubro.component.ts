import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PnotifyService } from 'src/app/services/pnotify.service';
import { Guid } from 'guid-typescript';
import { AccountRubro } from 'src/app/models/AccountRubro';
import { AccountGroup } from 'src/app/models/AccountGroup';
import { AccountGroupService } from 'src/app/services/account-group.service';
import { AccountRubroService } from 'src/app/services/account-rubro.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-account-rubro',
  templateUrl: './account-rubro.component.html',
  styleUrls: ['./account-rubro.component.css']
})
export class AccountRubroComponent implements OnInit {
  rubro: AccountRubro = new AccountRubro();
  groups: Array<AccountGroup> = new Array<AccountGroup>();

  constructor(private rubroServices: AccountRubroService, private grupoServices: AccountGroupService
    , private router: Router
    , private pnotifyService: PnotifyService
    , private activateRoute: ActivatedRoute
    , private translate: TranslateService) {
      this.translate.setDefaultLang("rubro-es");
     }

  ngOnInit() {
    this.loadBasicInfo();
    let id = this.activateRoute.snapshot.paramMap.get('id');

    if (id) {
      this.GetByGID(id);
    }
    else {
      this.addNew();
    }
  }

  loadBasicInfo() {
    this.grupoServices.GetAll(true).subscribe(res => {
      if (res.Success) {
        this.groups = res.Data;
      }
    });
  }

  GetByGID(id: string) {
    this.rubroServices.FindById(id, true).subscribe(res => {
      if (res.Success) {
        this.rubro = res.Data;
      }
    });
  }

  addNew() {
    this.rubro = new AccountRubro();
    this.rubro.GID = Guid.create().toString();
  }

  back() {
    this.router.navigateByUrl("/accRubros");
  }

  save() {
    if (this.validateRubro(this.rubro)) { return; }
    this.rubroServices.Save(this.rubro).subscribe(res => {
      if (!res.Success) {
        this.pnotifyService.showError("Error has occurred");
      }
      else {
        this.pnotifyService.showSuccess("The rubro has been saved");
        this.router.navigateByUrl("/accRubros");
      }
    });

  }

  validateRubro(rub: AccountRubro) {
    let error = false;
    //Validate name on Data Base
    if (rub.AccRubroCode == undefined || rub.AccRubroCode == "") {

    }

    if (rub.AccRubroCode == undefined || rub.AccRubroCode == "") {
      this.pnotifyService.showError("The field Code can't be empty");
      error = true;
    }

    if (rub.AccRubroName == undefined || rub.AccRubroName == "") {
      this.pnotifyService.showError("The field Name can't be empty");
      error = true;

    }

    if (rub.AccGroupGID == undefined || rub.AccGroupGID == "") {
      this.pnotifyService.showError("You need select a group");
      error = true;

    }

    return error;
  }

}
