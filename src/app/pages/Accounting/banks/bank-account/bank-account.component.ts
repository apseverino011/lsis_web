import { Component, OnInit } from '@angular/core';
import { GeneralTypeService } from 'src/app/services/general-type.service';
import { BankAccount } from 'src/app/models/BankAccounts';
import { GeneralTypes } from 'src/app/models/general-types';

declare const $: any;

@Component({
  selector: 'app-bank-account',
  templateUrl: './bank-account.component.html',
  styleUrls: ['./bank-account.component.css']
})
export class BankAccountComponent implements OnInit {
  accounts: Array<BankAccount> = new Array<BankAccount>();
  account: BankAccount = new BankAccount();
  accTypes: Array<GeneralTypes> = new Array<GeneralTypes>();

  constructor(private generalServices: GeneralTypeService) { }

  ngOnInit() {
    this.loadBasicInfo();
  }

  loadBasicInfo() {
    //Account Types
    this.generalServices.GetDetailsByParentCode("ACC").subscribe(res => {
      if (res.Success) {
        this.accTypes = res.Data;

      }
    });
  }

  addAccount() {
    $("#addEditAccount").modal();
  }

}
