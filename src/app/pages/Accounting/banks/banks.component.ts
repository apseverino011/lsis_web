import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Guid } from "guid-typescript";
import { TranslateService } from '@ngx-translate/core';
import { PnotifyService } from '../../../services/pnotify.service';
import { Bank } from 'src/app/models/Banks';

@Component({
  selector: 'app-banks',
  templateUrl: './banks.component.html',
  styleUrls: ['./banks.component.css']
})
export class BanksComponent implements OnInit {
  bank: Bank = new Bank;
  constructor(private router: Router) { }

  ngOnInit() {
  }

  save() {

  }

  back() {
    this.router.navigateByUrl("/banks");
  }

}
