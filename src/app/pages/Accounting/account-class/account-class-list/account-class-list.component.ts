import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AccountClassService } from 'src/app/services/account-class.service';
import { AccountClass } from 'src/app/models/AccClass';
import { AccountGroup } from 'src/app/models/AccountGroup';

@Component({
  selector: 'app-account-class-list',
  templateUrl: './account-class-list.component.html',
  styleUrls: ['./account-class-list.component.css']
})
export class AccountClassListComponent implements OnInit {

  classes: Array<AccountClass> = new Array<AccountClass>();

  constructor(private accountClassServices: AccountClassService, private router: Router, private translate: TranslateService) {
    translate.setDefaultLang('accClasses-es');
   }

  ngOnInit() {
    this.loadClasses()
  }

  loadClasses() {
    this.accountClassServices.GetAll(true).subscribe(res => {
      console.log(res);
      if (res.Success) {
        this.classes = res.Data;
      }
    });
  }

  edit(_class: AccountClass) {
    this.router.navigateByUrl("/accClass/edit/" + _class.GID);
  }

  remove(_class: AccountClass) {

  }

}
