import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Guid } from 'guid-typescript';
import Enums from 'src/app/models/Enums.json';
import { PnotifyService } from 'src/app/services/pnotify.service';
import { AccountClass } from 'src/app/models/AccClass';
import { AccountGroup } from 'src/app/models/AccountGroup';
import { AccountGroupService } from 'src/app/services/account-group.service';
import { AccountClassService } from 'src/app/services/account-class.service';

@Component({
  selector: 'app-account-class',
  templateUrl: './account-class.component.html',
  styleUrls: ['./account-class.component.css']
})
export class AccountClassComponent implements OnInit {

  accClass: AccountClass = new AccountClass();
  groups: Array<AccountGroup> = new Array<AccountGroup>();

  constructor(private router: Router, private activateRoute: ActivatedRoute
    , private groupServices: AccountGroupService
    , private accClassServices: AccountClassService
    , private pnotifyService: PnotifyService
    , private translate: TranslateService) { 
      translate.setDefaultLang('accClasses-es');
    }

  ngOnInit() {
    this.loadBasicInfo();
    let id = this.activateRoute.snapshot.paramMap.get('id');

    if (id) {
      this.GetByGID(id);
    }
    else {
      this.addNew();
    }
  }

  GetByGID(id: string) {
    this.accClassServices.FindById(id, true).subscribe(res => {
      if (res.Success) {
        this.accClass = res.Data;
      }
    });
  }

  addNew() {
    this.accClass = new AccountClass();
    this.accClass.GID = Guid.create().toString();
  }

  loadBasicInfo() {
    this.groupServices.GetAll(true).subscribe(res => {
      if (res.Success) {
        this.groups = res.Data;
      }
    });
  }

  back() {
    this.router.navigateByUrl('/accClasses');
  }

  onChange(groupId: string) {
    this.accClass.AccGroupId = groupId;
  }

  save() {
    this.validateClass(this.accClass);

    this.accClassServices.Save(this.accClass).subscribe(res => {
      if (res.Success) {
        this.pnotifyService.showSuccess("The class has been saved");
        this.router.navigateByUrl("/accClasses");
      } else {
        this.pnotifyService.showError("Error has occurred");
      }
    });
  }

  validateClass(_class: AccountClass) {
    let error = false;

    if (_class.AccClassName == undefined || _class.AccClassName == "") {
      this.pnotifyService.showError("The field name can't be empty");
      error = true;
    }

    if (_class.AccGroupId == undefined || _class.AccGroupId == "") {
      this.pnotifyService.showError("You need to be select a Group");
      error = true;
    }

    return error;
  }

}
