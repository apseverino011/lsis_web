import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountCatalogListComponent } from './account-catalog-list.component';

describe('AccountCatalogListComponent', () => {
  let component: AccountCatalogListComponent;
  let fixture: ComponentFixture<AccountCatalogListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountCatalogListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountCatalogListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
