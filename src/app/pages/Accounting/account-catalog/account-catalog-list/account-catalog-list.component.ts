import { Component, OnInit } from '@angular/core';
import { AccountCatalog } from 'src/app/models/AccCatalog';
import { AccountCatalogService } from 'src/app/services/account-catalog.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ParentAccount } from 'src/app/models/ParentAccount';

@Component({
  selector: 'app-account-catalog-list',
  templateUrl: './account-catalog-list.component.html',
  styleUrls: ['./account-catalog-list.component.css']
})
export class AccountCatalogListComponent implements OnInit {
  //catalogs: Array<AccountCatalog> = new Array<AccountCatalog>();
  catalogs: Array<ParentAccount> = new Array<ParentAccount>();
  constructor(private catalogServices: AccountCatalogService
            , private router: Router
            , private translateService: TranslateService) { 
              this.translateService.setDefaultLang("catalog-es");
            }

  ngOnInit() {
    this.loadCatalogs();
  }

  loadCatalogs() {
    this.catalogServices.GetParents().subscribe(res => {
      if (res.Success) {
        this.catalogs = res.Data;
      }
    });
  }

  edit(catId: string) {
    this.router.navigateByUrl("/accCatalog/edit/" + catId);
  }

  remove(cat: AccountCatalog) {
    cat.IsDeleted = true;
    cat.IsActive = false;

    this.catalogServices.Save(cat).subscribe(res => {
      if (res.Success) {
        this.router.navigateByUrl("/accCatalogs");
      }
    });
  }

}
