import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountCatalogComponent } from './account-catalog.component';

describe('AccountCatalogComponent', () => {
  let component: AccountCatalogComponent;
  let fixture: ComponentFixture<AccountCatalogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountCatalogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountCatalogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
