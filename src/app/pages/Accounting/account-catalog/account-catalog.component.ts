import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AccountCatalog } from 'src/app/models/AccCatalog';
import { AccountGroupService } from 'src/app/services/account-group.service';
import { AccountClassService } from 'src/app/services/account-class.service';
import { AccountCatalogService } from 'src/app/services/account-catalog.service';
import { AccountGroup } from 'src/app/models/AccountGroup';
import { AccountClass } from 'src/app/models/AccClass';
import { GeneralTypes } from 'src/app/models/general-types';
import { PnotifyService } from 'src/app/services/pnotify.service';
import { GeneralTypeService } from 'src/app/services/general-type.service';
import { AccountRubroService } from 'src/app/services/account-rubro.service';
import { AccountRubro } from 'src/app/models/AccountRubro';
import Enums from 'src/app/models/Enums.json';
import { stringLiteral } from 'babel-types';
import { Guid } from 'guid-typescript';
import { AsyncAction } from 'rxjs/internal/scheduler/AsyncAction';

declare const $: any;

@Component({
  selector: 'app-account-catalog',
  templateUrl: './account-catalog.component.html',
  styleUrls: ['./account-catalog.component.css']
})
export class AccountCatalogComponent implements OnInit {
  account: AccountCatalog = new AccountCatalog();
  subAccount: AccountCatalog = new AccountCatalog();

  subAccountClone: AccountCatalog = new AccountCatalog();
  //subAccounts: Array<AccountCatalog> = new Array<AccountCatalog>();

  //Basic Info
  groups: Array<AccountGroup> = new Array<AccountGroup>();
  classes: Array<AccountClass> = new Array<AccountClass>();
  rubros: Array<AccountRubro> = new Array<AccountRubro>();
  origins: Array<GeneralTypes> = new Array<GeneralTypes>();

  //Transition values
  groupNumber: string;
  classNumber: string;
  accNumber: string;

  constructor(private router: Router
    , private activeRouter: ActivatedRoute
    , private catalogServices: AccountCatalogService
    , private groupServices: AccountGroupService
    , private classServices: AccountClassService
    , private generalServices: GeneralTypeService
    , private rubrosServices: AccountRubroService
    , private pnotifyService: PnotifyService) { }

  ngOnInit() {
    this.loadBasicInfo();
    let id = this.activeRouter.snapshot.paramMap.get('id');

    if (id) {
      this.editAccount(id);

    } else {
      this.addNewAccount();
    }
  }

  editAccount(id: string) {
    this.catalogServices.FindById(id, true).subscribe(res => {
      if (res.Success) {
        this.account = res.Data;

        this.accNumber = this.account.AccNumber;

        this.groupNumber = this.account.AccGroup.AccGroupCode;

        this.classNumber = this.account.AccClass.AccClassCode;
      }
    });
  }

  addNewAccount() {
    this.account = new AccountCatalog();

    this.account.GID = Guid.create().toString();
  }

  loadBasicInfo() {
    //Group drop
    this.groupServices.GetAll(false).subscribe(res => {
      if (res.Success) {
        this.groups = res.Data;
      }
    });

    //Origins drop
    this.generalServices.GetDetailsByParentCode(Enums.AccCodes.AccountOrigin).subscribe(res => {
      if (res.Success) {
        this.origins = res.Data;
      }
    });
  }

  editSubAccount(subAcc: AccountCatalog) {

    this.subAccountClone = Object.assign({}, subAcc);

    this.subAccount = subAcc;
    $("#subAccount").modal();
  }

  onChangeGroup(gid: string) {
    if (gid != undefined && gid != 'undefined') {

      let group = this.groups.filter(x => x.GID == gid);

      //Set Account group
      this.account.AccGroupId = gid;

      //Get next account number
      this.groupServices.GetNextAccNumber(gid).subscribe(res => {
        if (res.Success) {
          this.account.AccNumber = res.Data;
          this.accNumber = res.Data;
          this.groupNumber = group[0].AccGroupCode;
        }
        else {
          this.pnotifyService.showError(res.Messages.Message);
        }
      });

      this.setClasses(gid);

      // //Get classes for group
      // //Classes drop
      // this.classServices.FindClassesByGroupId(gid, false).subscribe(res => {
      //   if (res.Success) {
      //     this.classes = res.Data;
      //   }
      //   else {
      //     this.pnotifyService.showError(res.Messages.Message);
      //   }
      // });

      //Get rubros for group
      //Rubros drop
      this.rubrosServices.FindRubrosByGroupId(gid, false).subscribe(res => {
        if (res.Success) {
          this.rubros = res.Data;
        }
        else {
          this.pnotifyService.showError(res.Messages.Message);
        }
      });

      //Set Origin
      this.account.AccOriginId = group[0].AccOriginId;
    }
    else {
      this.account.AccNumber = undefined;
      this.account.AccOriginId = undefined;
      this.account.AccClassId = undefined;
      this.account.AccRubroId = undefined;
      this.account.AccFullNumber = undefined;
      this.classes = undefined;
      this.rubros = undefined;
    }
  }

  async setClasses(gid: string) {

    //Get classes for group
    //Classes drop
    await this.classServices.FindClassesByGroupId(gid, false).subscribe(res => {
      if (res.Success) {
        this.classes = res.Data;
      }
      else {
        this.pnotifyService.showError(res.Messages.Message);
      }
    });
  }

  onChangeClass(gid: string) {
    if (gid != undefined && gid != 'undefined') {
      let _class = this.classes.filter(x => x.GID == gid);
      this.classNumber = _class[0].AccClassCode;
      //this.account.AccFullNumber += _class[0].AccClassCode + this.account.AccNumber + "0000";
    }
    else {
      this.classNumber = "";
    }
  }

  addSubAccount() {
    let baseAccNumber = this.groupNumber + '-' + this.classNumber + '-' + this.accNumber;

    if (this.groupNumber == undefined
      || this.groupNumber.trim() == ""
      || this.classNumber == undefined
      || this.classNumber.trim() == "") {
      this.pnotifyService.showError("You need select the values Group and Class before add sub account.");
      return;
    }


    this.subAccount = new AccountCatalog();
    this.subAccount.GID = Guid.create().toString();
    this.subAccount.ParentAccountId = this.account.GID;

    if (this.account.AccGroupId != undefined) {
      this.groupServices.GetNextSubAccNumber(this.account.AccGroupId).subscribe(res => {
        if (res.Success) {
          this.subAccount.AccNumber = res.Data;
          this.subAccount.AccGroupId = this.account.AccGroupId;
          this.subAccount.AccOriginId = this.account.AccOriginId;
          this.subAccount.AccClassId = this.account.AccClassId;

          //Set Account full number
          this.subAccount.AccFullNumber = baseAccNumber + '-' + res.Data;
        }
      });
    }
    $("#subAccount").modal();
  }

  saveSubAccount() {
    //Validate sub account information
    if (this.subAccount.AccName == undefined || this.subAccount.AccName.trim() == "") {
      this.pnotifyService.showError("The name can't be empty");
      return;
    }

    let indx = this.account.SubAccounts.findIndex(x => x.GID == this.subAccount.GID);

    if (indx != -1) {
      //Update sub account
      this.account.SubAccounts[indx] = this.subAccount;
      this.subAccountClone = undefined;
    }
    else {
      //Add new sub account
      this.account.SubAccounts.push(this.subAccount);
    }

    $("#subAccount").modal('hide');
  }

  cancelEditSubAccount() {
    if (this.subAccountClone.GID != undefined) {
      let indx = this.account.SubAccounts.findIndex(x => x.GID == this.subAccount.GID);

      //Cancel update sub account
      this.account.SubAccounts[indx] = this.subAccountClone;
      this.subAccountClone = undefined;

    }
    else {
      this.groupServices.SetNextSubAccNumber(this.account.AccGroupId).subscribe(res => {});
    }
  }

  remove(subAcc: AccountCatalog) {
    if (subAcc.Indx == undefined || subAcc.Indx == 0) {
      this.account.SubAccounts.splice(this.account.SubAccounts.indexOf(subAcc), 1);
    }
    else {
      subAcc.IsActive = false;
      subAcc.IsDeleted = true;
    }
  }

  back() {
    this.router.navigateByUrl("/accCatalogs");
  }

  save() {
    //Add validations before save
    if (this.validateAccout()) { return; }

    //Saving
    this.catalogServices.Save(this.account).subscribe(res => {
      if (res.Success) {
        this.pnotifyService.showSuccess("The account has been saved.");
        this.router.navigateByUrl('/accCatalogs');
      }
      else {
        this.pnotifyService.showError("The account can't bee saved.");
      }
    });

  }

  validateAccout() {
    let error = false;

    if (this.account.AccGroupId == undefined || this.account.AccGroupId == "") {
      this.pnotifyService.showError("The account group can't be empty");
      error = true;
    }

    if (this.account.AccClassId == undefined || this.account.AccClassId == "") {
      this.pnotifyService.showError("The account class can't be empty");
      error = true;
    }

    if (this.account.AccRubroId == undefined || this.account.AccRubroId == "") {
      this.pnotifyService.showError("The account rubro can't be empty");
      error = true;
    }

    if (this.account.AccName == undefined || this.account.AccName == "") {
      this.pnotifyService.showError("The account name can't be empty");
      error = true;
    }

    return error;
  }

}
