import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccPeriodListComponent } from './acc-period-list.component';

describe('AccPeriodListComponent', () => {
  let component: AccPeriodListComponent;
  let fixture: ComponentFixture<AccPeriodListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccPeriodListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccPeriodListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
