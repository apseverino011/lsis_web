import { Component, OnInit } from '@angular/core';
import { AccountPeriod } from 'src/app/models/AccountPeriod';
import { PeriodDto } from 'src/app/models/PeriodDto';
import { AccountPeriodsService } from 'src/app/services/account-periods.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-acc-period-list',
  templateUrl: './acc-period-list.component.html',
  styleUrls: ['./acc-period-list.component.css']
})
export class AccPeriodListComponent implements OnInit {

  periods: Array<PeriodDto> = new Array<PeriodDto>();

  constructor(private periodServices: AccountPeriodsService
            , private router: Router
            , private translate: TranslateService) {
              translate.setDefaultLang('periods-en'); }

  ngOnInit() {
    this.loadInfo();
  }

  loadInfo() {
    this.periodServices.GetPeriods().subscribe(res => {
      this.periods = res;
    });
  }

  editPeriod(period: PeriodDto) {
    this.router.navigateByUrl("/accPeriod/edit/"+period.PeriodId);
  }

  lockPeriod(period: PeriodDto) {

  }

}
