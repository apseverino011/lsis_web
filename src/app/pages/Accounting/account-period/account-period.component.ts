import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AccountPeriod } from 'src/app/models/AccountPeriod';
import { PnotifyService } from 'src/app/services/pnotify.service';
import { Guid } from 'guid-typescript';
import { Periods } from 'src/app/models/Periods';
import { PeriodStateService } from 'src/app/services/period-state.service';
import { PeriodState } from 'src/app/models/PeriodState';
import { TranslateService } from '@ngx-translate/core';
import { AccountPeriodsService } from 'src/app/services/account-periods.service';

@Component({
  selector: 'app-account-period',
  templateUrl: './account-period.component.html',
  styleUrls: ['./account-period.component.css']
})

export class AccountPeriodComponent implements OnInit {
  accountPeriod: AccountPeriod = new AccountPeriod();
  periodState: Array<PeriodState> = new Array<PeriodState>();

  startYears: Array<number> = new Array<number>();
  endYears: Array<number> = new Array<number>();
  _startYear: number;
  _startMonth: number;
  _endYear: number;
  _endMonth: number;
  constructor(private router: Router
    , private pnotifyService: PnotifyService
    , private activateRoute: ActivatedRoute
    , private periodStateServices: PeriodStateService
    , private periodSerevices: AccountPeriodsService
    , private translate: TranslateService
    , private perdiodServices: AccountPeriodsService) {
    translate.setDefaultLang('periods-es');
    let year = new Date().getFullYear();

    for (let i = (year - 1); i <= (year + 10); i++) {
      this.startYears.push(i);
    }

  }

  ngOnInit() {
    this.loadBasicInfo();
    let id = this.activateRoute.snapshot.paramMap.get('id');

    if (id) {
      this.getByGID(id);
    }
    else {
      this.addNew();
    }
  }

  loadBasicInfo() {
    //Load period state
    this.periodStateServices.GetAll().subscribe(res => {
      if (res.Success) {
        this.periodState = res.Data;
      }
    });

  }

  getByGID(id: string) {
    this.periodSerevices.Find(id).subscribe(res => {
      if (res.Success) {
        this.accountPeriod = res.Data;
      }
    });
  }

  addNew() {
    this.accountPeriod = new AccountPeriod();
    this.accountPeriod.GID = Guid.create().toString();
  }

  back() {
    this.router.navigateByUrl('/accPeriods');
  }

  save() {
    if (this.validatePeriod()) { return; }
    this.periodSerevices.Save(this.accountPeriod).subscribe(res => {
      console.log(res);
    })
  }

  validatePeriod() {
    let error = false;

    if (this.accountPeriod.PeriodDetails.length <= 0) {
      this.pnotifyService.showError("The period need be generate");
      error = true;
    }

    if (this.accountPeriod.StartDate == undefined || this.accountPeriod.StartDate == "") {
      this.pnotifyService.showError("The start period date can't be empty");
      error = true;
    }

    if (this.accountPeriod.EndDate == undefined || this.accountPeriod.EndDate == "") {
      this.pnotifyService.showError("The end period date can't be empty");
      error = true;
    }

    if (this.accountPeriod.Name == undefined || this.accountPeriod.Name == "") {
      this.pnotifyService.showError("The period name can't be empty");
      error = true;
    }

    return error;
  }

  refresh() {

  }

  onChangeStartMonth(month: any) {
    if (this._startMonth != undefined && this._startYear != undefined) {
      this.accountPeriod.StartDate = (this._startMonth + "" + this._startYear);
    }

    if (this._startMonth != undefined && this._startYear != undefined && this._endMonth != undefined && this._endYear != undefined) {
      this.accountPeriod.Name = (this._startMonth + this._startYear + "-" + this._endMonth + this._endYear);
    }
  }

  onChangeStartYear(year: string) {
    //debugger;
    if (this._startMonth != undefined && this._startYear != undefined) {
      this.accountPeriod.StartDate = (this._startMonth + "" + this._startYear);
    }

    if (this._startMonth != undefined && this._startYear != undefined && this._endMonth != undefined && this._endYear != undefined) {
      this.accountPeriod.Name = (this._startMonth + this._startYear + "-" + this._endMonth + this._endYear);
    }

    //Reset array
    this.endYears = new Array<number>();

    let _year = parseInt(year);

    for (let i = _year; i <= (_year + 10); i++) {
      this.endYears.push(i);
    }
  }

  onChangeEndMonth(month: any) {
    if (this._endMonth != undefined && this._endYear != undefined) {
      this.accountPeriod.EndDate = (this._endMonth + "" + this._endYear);
    }

    if (this._startMonth != undefined && this._startYear != undefined && this._endMonth != undefined && this._endYear != undefined) {
      this.accountPeriod.Name = (this._startMonth + this._startYear + "-" + this._endMonth + this._endYear);
    }
  }

  onChangeEndYear(year: string) {
    if (this._endMonth != undefined && this._endYear != undefined) {
      this.accountPeriod.EndDate = (this._endMonth + "" + this._endYear);
    }

    if (this._startMonth != undefined && this._startYear != undefined && this._endMonth != undefined && this._endYear != undefined) {
      this.accountPeriod.Name = (this._startMonth + this._startYear + "-" + this._endMonth + this._endYear);
    }
  }

  onChangePeriodState(periodState: string) {
    let ps = this.accountPeriod.PeriodDetails.length;
    if (ps > 0) {
      this.accountPeriod.PeriodDetails.forEach(res => {
        res.PeriodState = periodState;
      });
    }
  }

  generatePeriods() {
    if (this.validatePeriods()) { return; }
    //Reset periods
    this.accountPeriod.PeriodDetails = new Array<Periods>();

    if (this._startYear == this._endYear) {
      for (let i = this._startMonth; i <= this._endMonth; i++) {
        //create a new period
        let period = new Periods();
        period.GID = Guid.create().toString();
        period.PeriodId = this.accountPeriod.GID;
        period.Name = ((i <= 9 ? "0" + parseInt(i.toString()) : i) + "" + this._startYear);
        period.LogDate = new Date();
        if (this.accountPeriod.PeriodState != undefined) {
          period.PeriodState = this.accountPeriod.PeriodState;
        }

        this.accountPeriod.PeriodDetails.push(period);

      }
    }
    else {
      //Start period year
      for (let i = this._startMonth; i <= 12; i++) {
        //create a new period
        let period = new Periods();
        period.GID = Guid.create().toString();
        period.PeriodId = this.accountPeriod.GID;
        period.Name = ((i <= 9 ? "0" + parseInt(i.toString()) : i) + "" + this._startYear);
        period.LogDate = new Date();
        if (this.accountPeriod.PeriodState != undefined) {
          period.PeriodState = this.accountPeriod.PeriodState;
        }

        this.accountPeriod.PeriodDetails.push(period);
      }

      //End period year
      for (let i = 1; i <= this._endMonth; i++) {
        //create a new period
        let period = new Periods();
        period.GID = Guid.create().toString();
        period.PeriodId = this.accountPeriod.GID;
        period.Name = ((i <= 9 ? "0" + parseInt(i.toString()) : i) + "" + this._endYear);
        period.LogDate = new Date();
        if (this.accountPeriod.PeriodState != undefined) {
          period.PeriodState = this.accountPeriod.PeriodState;
        }

        this.accountPeriod.PeriodDetails.push(period);
      }

    }
  }

  validatePeriods() {
    let error = false;

    if (this._startYear == this._endYear) {
      if (this._startMonth > this._endMonth) {
        this.pnotifyService.showError("The start month can't be higher to end month, in the same year");
        error = true;

      }
    }

    if (this._startMonth == undefined) {
      this.pnotifyService.showError("The start month need be selected");
      error = true;
    }

    if (this._startYear == undefined) {
      this.pnotifyService.showError("The start year need be selected");
      error = true;
    }

    if (this._endMonth == undefined) {
      this.pnotifyService.showError("The end month need be selected");
      error = true;
    }

    if (this._endYear == undefined) {
      this.pnotifyService.showError("The end year need be selected");
      error = true;
    }

    return error;
  }

}
