import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Guid } from 'guid-typescript';
import Enums from 'src/app/models/Enums.json';
import { PnotifyService } from 'src/app/services/pnotify.service';
import { AccountGroup } from 'src/app/models/AccountGroup';
import { GeneralTypes } from 'src/app/models/general-types';
import { GeneralTypeService } from 'src/app/services/general-type.service';
import { AccountGroupService } from 'src/app/services/account-group.service';

@Component({
  selector: 'app-account-group',
  templateUrl: './account-group.component.html',
  styleUrls: ['./account-group.component.css']
})
export class AccountGroupComponent implements OnInit {
  group: AccountGroup = new AccountGroup();
  accTypes: Array<GeneralTypes> = new Array<GeneralTypes>();
  accOrigins: Array<GeneralTypes> = new Array<GeneralTypes>()

  constructor(private router: Router, private activateRoute: ActivatedRoute, private groupServices: AccountGroupService, private generalService: GeneralTypeService
    , private pnotifyService: PnotifyService, private translate: TranslateService) {
    translate.setDefaultLang('group-es');
  }

  ngOnInit() {
    this.loadDefaultValues();
    let id = this.activateRoute.snapshot.paramMap.get('id');

    if (id) {
      this.GetByGID(id);
    }
    else {
      this.addNew();
    }
  }

  loadDefaultValues() {
    let atCode = Enums.AccCodes.AcountTypes;

    this.generalService.GetDetailsByParentCode(atCode).subscribe(res => {
      if (res.Success) {
        this.accTypes = res.Data;
      }
    });

    //Origins
    this.generalService.GetDetailsByParentCode(Enums.AccCodes.AccountOrigin).subscribe(res => {
      if (res.Success) {
        this.accOrigins = res.Data;
      }
    });
  }

  GetByGID(id: string) {
    this.groupServices.FindById(id, false).subscribe(res => {
      if (res.Success) {
        this.group = res.Data;
      }
    });
  }

  addNew() {
    this.group = new AccountGroup();
    this.group.GID = Guid.create().toString();
  }

  back() {
    this.router.navigateByUrl('/accGroups');
  }

  save() {
    if (this.validateAccountGroup(this.group)) { return; }

    this.groupServices.Save(this.group).subscribe(res => {
      if (!res.Success) {
        this.pnotifyService.showError(res.Messages.Message);
      } else {
        this.pnotifyService.showSuccess("The group has been saved.");
        this.router.navigateByUrl("/accGroups");
      }
    });
  }

  onChange(accTypeId: string) {
    this.group.AccTypeId = accTypeId;

  }

  validateAccountGroup(gp: AccountGroup) {
    let error = false;
    if (gp.AccGroupName == undefined || gp.AccGroupName == "") {
      this.pnotifyService.showError("The field name can't be empty");
      error = true;
    }

    if (gp.AccOriginId == undefined || gp.AccOriginId == "") {
      this.pnotifyService.showError("You need to select an Account Origin");
      error = true;
    }

    if (gp.AccTypeId == undefined || gp.AccTypeId == "") {
      this.pnotifyService.showError("You need to select an Account Type");
      error = true;
    }

    return error;
  }
}
