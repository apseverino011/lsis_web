import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { PnotifyService } from 'src/app/services/pnotify.service';
import { AccountGroup } from 'src/app/models/AccountGroup';
import { AccountGroupService } from 'src/app/services/account-group.service';

@Component({
  selector: 'app-account-group-list',
  templateUrl: './account-group-list.component.html',
  styleUrls: ['./account-group-list.component.css']
})
export class AccountGroupListComponent implements OnInit {

  groups: Array<AccountGroup> = new Array<AccountGroup>();

  constructor(private accountGroupServices: AccountGroupService, private pnotifyService: PnotifyService
            , private router: Router, private translate: TranslateService) { 
              translate.setDefaultLang('group-es');
            }

  ngOnInit() {
    this.loadGroups();
  }

  loadGroups() {
    this.accountGroupServices.GetAll(true).subscribe(res => {
      if (res.Success) {
        this.groups = res.Data;
      }
      else {
        //this.pnotifyService.showError(res.Messages.Message);
      }
    });
  }

  edit(group: AccountGroup){
    this.router.navigateByUrl('/accGroup/edit/' + group.GID)
  }

  remove(group: AccountGroup){

  }

}
