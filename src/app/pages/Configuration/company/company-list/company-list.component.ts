import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Company } from 'src/app/models/Company';
import { CompanyService } from 'src/app/services/company.service';
import { PnotifyService } from '../../../../services/pnotify.service';

@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.css']
})
export class CompanyListComponent implements OnInit {
  companies: Array<Company> = new Array<Company>();

  constructor(private companyServices: CompanyService, private router: Router, private activatedRoute: ActivatedRoute, private pnotifyService: PnotifyService, private spinner: NgxSpinnerService
    , private translateService: TranslateService) {
    this.translateService.use('company-es');
  }

  ngOnInit() {
    this.spinner.show();
    this.companyServices.GetAll().subscribe(res => {
      if (res.Success) {
        this.companies = res.Data;
        this.spinner.hide();
      }
    });
  }

  edit(comp: Company) {
    this.router.navigateByUrl('/company/edit/' + comp.GID);
  }

  remove(comp: Company) {
    comp.IsDeleted = true;
    comp.IsActive = false;

    this.companyServices.Save(comp).subscribe(res => {
      if(!res.Success){
      this.pnotifyService.showError("The company can't be remove");
      }
      else{
        this.pnotifyService.showSuccess("The has been remove correctly");
        this.router.navigateByUrl('/companies');
      }
    });
  }

}
