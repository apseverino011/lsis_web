import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { PnotifyService } from '../../../services/pnotify.service';
import { Company } from 'src/app/models/Company';
import { GeneralModules } from 'src/app/models/GeneralModule';
import { ActivatedRoute, Router } from '@angular/router';
import { ModulesService } from 'src/app/services/modules.service';
import { CompanyService } from 'src/app/services/company.service';
import { Guid } from 'guid-typescript';

declare const $: any;

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent implements OnInit {
  company: Company = new Company();
  modules: Array<GeneralModules> = new Array<GeneralModules>();

  constructor(private translateService: TranslateService, private router: Router, private moduleServices: ModulesService
    , private activateRoute: ActivatedRoute, private pnotifyService: PnotifyService
    , private companyServices: CompanyService) {
    this.translateService.use('company-es');
  }

  ngOnInit() {
    let id = this.activateRoute.snapshot.paramMap.get('id');

    if (id) {
      this.GetCompanyByGID(id);
    }
    else {
      this.addNewCompany();
    }
  }

  GetCompanyByGID(id: string) {
    this.companyServices.GetCompanyById(id).subscribe(res => {
      if(res.Success)
      this.company = res.Data;
      
      this.loadBasicInfo();
    });
  }

  addNewCompany() {
    this.company = new Company();
    this.company.GID = Guid.create().toString();

    this.loadBasicInfo();
  }

  loadBasicInfo() {
    this.moduleServices.GetMainModule().subscribe(res => {
      if (res.Success) {
        this.modules = res.Data;

        if (this.company.Modules.length > 0) {
          let gids = this.company.Modules.map(x => x.GID);
          this.modules.forEach(val => {
            if (gids.includes(val.GID)) {
              val.Select = true;
            }
          });
        }

      }
    });
  }

  AddRemoveModuleToList(mod: GeneralModules) {
    let index = this.company.Modules.findIndex(x => x.GID == mod.GID);

    if (index == -1) {
      this.company.Modules.push(mod);
    }
    else {
      this.company.Modules.splice(index, 1);
    }
  }

  removeModule(mod: GeneralModules) {
    let index = this.company.Modules.findIndex(x => x.GID == mod.GID);

    this.company.Modules.splice(index, 1);
  }

  addModule() {
    $("#modu").modal();
  }

  finishModal() {
    $("#modu").modal('hide');
  }


  back() {
    this.router.navigateByUrl('/companies');
  }

  save() {
    if (this.validateCompany()) { return; }
    this.companyServices.Save(this.company).subscribe(res => {
      if (res.Success) {
        this.pnotifyService.showSuccess("The company has been saved");
        this.router.navigateByUrl('/companies');

      }
    });
  }

  validateCompany() {
    let error = false;

    if (this.company.RNC == undefined || this.company.RNC == "") {
      this.pnotifyService.showError("The field RNC is required");
      error = true;
    }

    if (this.company.Code == undefined || this.company.Code == "") {
      this.pnotifyService.showError("The field Code is required");
      error = true;
    }

    if (this.company.BusinessName == undefined || this.company.BusinessName == "") {
      this.pnotifyService.showError("The field Business Name is required");
      error = true;
    }

    if (this.company.Description == undefined || this.company.Description == "") {
      this.pnotifyService.showError("The field description is required");
      error = true;
    }

    /*if (this.company.Url == undefined || this.company.Url == "") {
      this.pnotifyService.showError("The field Url is required");
      error = true;
    }*/

    if (this.company.ContactNumbers == undefined || this.company.ContactNumbers.length == 0) {
      this.pnotifyService.showError("You need at least a contact number");
      error = true;
    }

    /*if (this.company.Locations == undefined || this.company.Locations.length == 0) {
      this.pnotifyService.showError("You need at least a location");
      error = true;
    }*/

    if (this.company.ContactPersons == undefined || this.company.ContactPersons.length == 0) {
      this.pnotifyService.showError("You need at least a contact person");
      error = true;
    }

    if (this.company.Modules == undefined || this.company.Modules.length == 0) {
      this.pnotifyService.showError("You need at least a module");
      error = true;
    }

    return error;
  }

}
