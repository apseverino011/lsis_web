import { Component, TemplateRef, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Guid } from "guid-typescript";
import { TranslateService } from '@ngx-translate/core';
import { PnotifyService } from '../../../services/pnotify.service';
import { RolesService } from '../../../services/roles.service'
import { throwStatement } from 'babel-types';
import { UserRol, RolesModulesOptions } from 'src/app/models/Roles';
import { GeneralModules, Options } from 'src/app/models/GeneralModule';
import { _appIdRandomProviderFactory } from '@angular/core/src/application_tokens';

declare const $: any;

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {

  userRole: UserRol = new UserRol();
  generalModules: GeneralModules = new GeneralModules();
  optionsModules: Options = new Options();
  gModules: Array<GeneralModules> = new Array<GeneralModules>();
  mOptions: Array<Options> = new Array<Options>();
  mSubOptions: Options = new Options();
  moduleSelect: boolean;
  showOptions: boolean;
  showSubOptions: boolean;

  //Create a new Option
  rolesModulesOptions: RolesModulesOptions = new RolesModulesOptions();

  constructor(private roleServices: RolesService, private router: Router, private activatedRoute: ActivatedRoute, private pnotifyService: PnotifyService, private spinner: NgxSpinnerService
    , private translate: TranslateService) {
    translate.use('role-es');
    this.showOptions = false;
  }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');

    if (id) {
      this.GetByGID(id);
    }
    else {
      this.addNewRole();
    }
  }

  addOrRemoveModule(mod: GeneralModules) {
    let _mod = this.userRole.Options.find(x => x.ModuleId == mod.GID);

    if (_mod) {
      _mod.IsActive = !_mod.IsActive;
      _mod.IsDeleted = !_mod.IsDeleted;
      mod.Options.forEach(x => { x.Select = false; if (x.IsSubM) { x.Options.forEach(cc => { cc.Select = false; }); } });
    }
  }

  addOrRemoveOptions(obj: any) {
    let exist = this.userRole.Options.find(x => x.OptionId == obj.GID);

    if (exist == undefined) {
      //New Option
      this.rolesModulesOptions = new RolesModulesOptions();
      this.rolesModulesOptions.GID = Guid.create().toString();
      this.rolesModulesOptions.IsMain = true;
      this.rolesModulesOptions.UserRoleId = this.userRole.GID;
      this.rolesModulesOptions.ModuleId = this.generalModules.GID;
      this.rolesModulesOptions.OptionId = obj.GID;

      this.userRole.Options.push(this.rolesModulesOptions);
    }
    else if (exist.Indx == 0) {
      this.userRole.Options.splice(this.userRole.Options.indexOf(obj), 1);
    }
    else {
      exist.IsActive = !exist.IsActive;
      exist.IsDeleted = !exist.IsDeleted;
    }
  }

  addOrRemoveSubOptions(obj: any) {
    obj.Select = true;
    this.mSubOptions = obj.Options;

    let exist = this.userRole.Options.find(x => x.OptionId == obj.GID);

    if (exist == undefined) {
      //New Option
      this.rolesModulesOptions = new RolesModulesOptions();
      this.rolesModulesOptions.GID = Guid.create().toString();
      this.rolesModulesOptions.IsSub = true;
      this.rolesModulesOptions.UserRoleId = this.userRole.GID;
      this.rolesModulesOptions.ModuleId = this.optionsModules.GID;
      this.rolesModulesOptions.OptionId = obj.GID;

      this.userRole.Options.push(this.rolesModulesOptions);
    }
    else if (exist.Indx == 0) {
      this.userRole.Options.splice(this.userRole.Options.indexOf(obj), 1);
    }
    else {
      exist.IsActive = !exist.IsActive;
      exist.IsDeleted = !exist.IsDeleted;
    }

  }

  modules(obj: any) {
    this.showOptions = true;
    this.showSubOptions = false;

    //Change site for table
    let modules = document.getElementById('modulesDiv');
    modules.className = "col-sm-6 col-md-6";

    let option = document.getElementById('optionsDiv');
    if (option != undefined) { option.className = "col-sm-6 col-md-6" };
    //Set module
    this.generalModules = obj;

    //Set options available
    this.mOptions = obj.Options;
  }

  options(obj: any) {
    this.optionsModules = obj;

    if (obj.IsSubM) {
      this.showSubOptions = true;
      this.mSubOptions = obj.Options;

      let modules = document.getElementById('modulesDiv');
      modules.className = "col-sm-6 col-md-4";

      let options = document.getElementById('optionsDiv');
      options.className = "col-sm-6 col-md-4";
    }

  }

  addNewRole() {
    this.userRole.GID = Guid.create().toString();

    this.roleServices.GetModulosByCompany().subscribe(res => {

      if (res.Success) {
        this.gModules = res.Data;
      }
      else {
        this.pnotifyService.showError(res.Messages.Message);
      }
    });

  }

  GetByGID(gid: string) {
    this.roleServices.GetUserRoleToEdit(gid).subscribe(res => {
      if (res.Success) {
        this.userRole = res.Data;
        this.gModules = this.userRole.EditOptions;
      }
      else {
        this.pnotifyService.showError(res.Messages.Message);
      }
    });
  }

  save() {
    if (this.ValidarOptionsValues()) { return; }

    this.roleServices.Save(this.userRole).subscribe(res => {
      if (res.Success) {
        this.pnotifyService.showSuccess('The role has been saved');
        this.router.navigateByUrl("/roles");
      }
      else {
        this.pnotifyService.showError(res.Messages.Message);
      }
    });

  }

  back() {
    this.router.navigateByUrl("/roles");
  }

  ValidarOptionsValues() {
    let hayErrores = false;
    let modules = this.gModules.filter(x => x.Select == true);
    debugger;
    /*this.userRole.ValidateRoleName(this.options.Name, this.options.Owner).subscribe(res => {
      if (res.Success && this.options.Indx <= 0) {
        this.pnotifyService.showError('Already exists a option with the same name, for the module '+this.generalModules.Name);
        hayErrores = true;
      }
    });*/

    if (this.userRole.Name == undefined || this.userRole.Name == "") {
      this.pnotifyService.showError('The field Name cant be empty');
      hayErrores = true;
    }

    if (modules.length <= 0) {
      this.pnotifyService.showError('The need at least 1 module to bew create.');
      hayErrores = true;
    }

    if (modules) {
      modules.forEach(cc => {
        if (cc.Options.filter(xc => xc.Select == true).length <= 0) {

          hayErrores = true;
          this.pnotifyService.showError('The need at least 1 option for the module.');
        }
        cc.Options.forEach(cx => {
          if ((cx.Select == true && cx.IsSubM == true) && cx.Options.filter(cxc => cxc.Select == true).length <= 0) {

            hayErrores = true;
            this.pnotifyService.showError('The need at least 1 option for the Sub-module.');
          }
        });
      });
    }

    return hayErrores;
  }

}
