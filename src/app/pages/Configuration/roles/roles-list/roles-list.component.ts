import { Component, TemplateRef, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Guid } from "guid-typescript";
import { TranslateService } from '@ngx-translate/core';
import { PnotifyService } from '../../../../services/pnotify.service';
import { RolesService } from '../../../../services/roles.service'
import { throwStatement } from 'babel-types';
import { UserRol } from 'src/app/models/Roles';

@Component({
  selector: 'app-roles-list',
  templateUrl: './roles-list.component.html',
  styleUrls: ['./roles-list.component.css']
})
export class RolesListComponent implements OnInit {

  userRoles: Array<UserRol> = new Array<UserRol>();

  constructor(private roleServices: RolesService, private router: Router, private activatedRoute: ActivatedRoute, private pnotifyService: PnotifyService, private spinner: NgxSpinnerService
    , private translate: TranslateService) {
    translate.use('role-es');
  }

  ngOnInit() {
    this.spinner.show();

    this.roleServices.GetUserRoles().subscribe(res => {
      if (res.Success) {
        this.userRoles = res.Data;
      }
    });

    this.spinner.hide();
  }

  edit(sm: UserRol){
    this.router.navigateByUrl('/role/edit/' + sm.GID)
  }

  remove(role: UserRol) {
    role.IsActive = false;
    role.IsDeleted = true;

    this.roleServices.Save(role).subscribe(res => {

      if (!res.Success) {
        this.pnotifyService.showError("The role can't be remove.");
      } else {
        this.pnotifyService.showSuccess("The role remove correctly.");
        this.ngOnInit();
      }

    });

  }

}
