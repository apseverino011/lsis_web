import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TgeneralComponent } from './tgeneral.component';

describe('TgeneralComponent', () => {
  let component: TgeneralComponent;
  let fixture: ComponentFixture<TgeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TgeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TgeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
