import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { GeneralTypeService } from '../../../../services/general-type.service';
import { GeneralTypes } from '../../../../models/general-types';

@Component({
  selector: 'app-tgeneral-list',
  templateUrl: './tgeneral-list.component.html',
  styleUrls: ['./tgeneral-list.component.css']
})
export class TgeneralListComponent implements OnInit {

  generalTypes: Array<GeneralTypes> = new Array<GeneralTypes>();

  constructor(private translate: TranslateService, private gtypeServices: GeneralTypeService, private router: Router) {
    translate.use('tgeneral-es');
  }

  ngOnInit() {
    this.gtypeServices.GetAllParents(true).subscribe( res=> {
      if(res.Success){
        this.generalTypes = res.Data;
      }
    });
  }
  
  edit(object: GeneralTypes) {
    this.router.navigateByUrl('/gtypes/edit/' + object.GID)
  }

}
