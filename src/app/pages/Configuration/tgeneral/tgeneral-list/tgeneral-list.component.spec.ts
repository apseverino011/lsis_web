import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TgeneralListComponent } from './tgeneral-list.component';

describe('TgeneralListComponent', () => {
  let component: TgeneralListComponent;
  let fixture: ComponentFixture<TgeneralListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TgeneralListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TgeneralListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
