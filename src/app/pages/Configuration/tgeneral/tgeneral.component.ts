import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Guid } from 'guid-typescript';
import { PnotifyService } from '../../../services/pnotify.service';
import { GeneralTypes } from 'src/app/models/general-types';
import { GeneralTypeService } from 'src/app/services/general-type.service';

declare const $: any;

@Component({
  selector: 'app-tgeneral',
  templateUrl: './tgeneral.component.html',
  styleUrls: ['./tgeneral.component.css']
})
export class TgeneralComponent implements OnInit {

  generalType: GeneralTypes = new GeneralTypes();
  chield: GeneralTypes = new GeneralTypes();
  chieldClone: GeneralTypes = new GeneralTypes();

  constructor(private translate: TranslateService, private activateRoute: ActivatedRoute, private spinner: NgxSpinnerService
    , private gtypeServices: GeneralTypeService, private router: Router, private pnotifyService: PnotifyService) {
    translate.use('tgeneral-es');
  }

  ngOnInit() {
    let id = this.activateRoute.snapshot.paramMap.get('id');

    if (id) {
      this.GetByGID(id);
    }
    else {
      this.addNewGType();
    }
  }

  save() {
    if (this.validateGeneralType()) { return; }
    this.spinner.show();
    this.gtypeServices.Save(this.generalType).subscribe(res => {
      if (res.Success) {
        this.pnotifyService.showSuccess('The type have been saved correctly');
        this.router.navigateByUrl('/gtypes');
      }
      else {
        this.pnotifyService.showError(res.Messages.Message);
      }
    });

    this.spinner.hide();
  }

  cancel() {

  }

  editOpt(obj: GeneralTypes) {

    this.chieldClone = Object.assign({}, obj);

    this.chield = obj;

    $('#option').modal();
  }

  removeOpt(obj: GeneralTypes) {
    if (obj.Indx >= 0) {
      obj.IsDeleted = true;
      obj.IsActive = false;
    }
    else {
      this.generalType.Chields.splice(this.generalType.Chields.indexOf(obj), 1);
    }
  }

  GetByGID(id: string) {
    this.gtypeServices.FindByGID(id, true).subscribe(res => {
      if (res.Success) {
        this.generalType = res.Data;
      }
    });
  }

  addNewGType() {
    this.generalType = new GeneralTypes();
    this.generalType.GID = Guid.create().toString();
    this.generalType.IsParent = true;
  }

  AddNewOption(obj: any) {
    this.chield = new GeneralTypes();
    this.chield.GID = Guid.create().toString();
    this.chield.OwnerId = obj.GID;
    this.chield.Indx = -1;

    $("#option").modal();

  }

  saveNewOption(obj: GeneralTypes) {
    if (this.validateChields()) { return; }

    let index = this.generalType.Chields.findIndex(x => x.GID == obj.GID);

    if (index == -1) {
      if (obj.Code != undefined) { obj.Code = this.generalType.Code + '-' + obj.Code; }

      obj.StatusName = 'Activo';

      this.generalType.Chields.push(obj);
      this.chield.Indx = 0;
    }

    $("#option").modal('hide');
  }

  cancelOption(obj: GeneralTypes) {
    if (obj.Indx <= 0) {
      this.chield = undefined;
    }
    else {
      let index = this.generalType.Chields.findIndex(x => x.GID == obj.GID);
      this.generalType.Chields[index] = this.chieldClone;
    }

    $("#option").modal('hide');
  }

  validateGeneralType() {
    let hayErrores = false;

    if (this.generalType.Code == undefined || this.generalType.Code == "") {
      this.pnotifyService.showError('The field Code cant be empty');
      hayErrores = true;
    }

    if (this.generalType.Description == undefined || this.generalType.Description == "") {
      this.pnotifyService.showError('The field Desciption cant be empty');
      hayErrores = true;
    }

    if (this.generalType.Chields.length <= 0) {
      this.pnotifyService.showError('Needed add at least 1 option');
      hayErrores = true;
    }

    return hayErrores;
  }

  validateChields() {
    let hayErrores = false;

    if (this.chield.Code == undefined || this.chield.Code == "") {
      this.pnotifyService.showError('The field Code cant be empty');
      hayErrores = true;
    }

    if (this.chield.Description == undefined && this.chield.Description == "") {
      this.pnotifyService.showError('The field Desciption cant be empty');
      hayErrores = true;
    }

    return hayErrores;
  }
}
