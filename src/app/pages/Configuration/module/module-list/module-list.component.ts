import { Component, TemplateRef, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { GeneralModules, Options } from '../../../../models/GeneralModule';
import { Guid } from "guid-typescript";
import { TranslateService } from '@ngx-translate/core';
import { PnotifyService } from '../../../../services/pnotify.service';
import { GeneralModulesService } from '../../../../services/general-modules.service'
import { throwStatement } from 'babel-types';


@Component({
  selector: 'app-module-list',
  templateUrl: './module-list.component.html',
  styleUrls: ['./module-list.component.css']
})
export class ModuleListComponent implements OnInit {

  generalModules: Array<GeneralModules> = new Array<GeneralModules>();

  constructor(private generalServices: GeneralModulesService, private router: Router, private activatedRoute: ActivatedRoute, private pnotifyService: PnotifyService, private spinner: NgxSpinnerService
    , private translate: TranslateService) {
    translate.setDefaultLang('module-es');
  }

  ngOnInit() {
    this.spinner.show();

    this.generalServices.GetAllMainModules(true).subscribe(res => {
      if (res.Success) {
        this.generalModules = res.Data;
      }
      else {
        this.pnotifyService.showError(res.Messages.Message);
      }
    });

    this.spinner.hide();
  }

  edit(object: GeneralModules) {
    this.router.navigateByUrl('/module/edit/' + object.GID)
  }

  remove(mod: GeneralModules) {
    mod.IsDeleted = true;
    mod.IsActive = false;

    this.generalServices.Save(mod).subscribe(res => {
      if (!res.Success) {
        this.pnotifyService.showError("The module can't be remove");
      }
      else {
        this.pnotifyService.showSuccess("The module remove correctly");
        this.ngOnInit();
      }
    });

  }

}
