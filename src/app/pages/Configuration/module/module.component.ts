import { Component, TemplateRef, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { GeneralModules, Options } from '../../../models/GeneralModule';
import { Guid } from "guid-typescript";
import { TranslateService } from '@ngx-translate/core';
import { PnotifyService } from '../../../services/pnotify.service';
import { GeneralModulesService } from '../../../services/general-modules.service'
import { throwStatement } from 'babel-types';

declare const $: any;

@Component({
  selector: 'app-module',
  templateUrl: './module.component.html',
  styleUrls: ['./module.component.css']
})
export class ModuleComponent implements OnInit {

  generalModules: GeneralModules = new GeneralModules();
  options: Options = new Options();
  subOption: Options = new Options();
  optionClone: Options = new Options();
  subOptionClone: Options = new Options();
  addOption = false;
  lang = "";
  id: string;

  constructor(private generalServices: GeneralModulesService, private pnotifyService: PnotifyService, private spinner: NgxSpinnerService
    , private activateRoute: ActivatedRoute, private router: Router, private translate: TranslateService) {
    translate.setDefaultLang('module-es');
    //this.lang = translate.currentLang;
    this.id = activateRoute.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    if (this.id) {
      this.GetByGID(this.id);
    }
    else {
      this.addNewModule();
    }

  }

  refresh() {
    alert(this.router.url.substring(1, 13));
    this.router.navigateByUrl('/module', { skipLocationChange: true }).then(() =>
      this.router.navigate([this.router.url.substring(1, 13)]));
  }

  GetByGID(gid: string) {
    this.spinner.show();

    this.generalServices.GetByGID(gid).subscribe(res => {
      if (res.Success) {
        this.generalModules = res.Data;
        this.addOption = true;
      }
      else {
        this.pnotifyService.showError(res.Messages.Message);
      }
    });

    this.spinner.hide();
  }

  cancel() {
    this.addOption = false;

    this.generalModules = undefined;
  }

  save() {
    this.spinner.show();

    if (this.ValidarGeneralModule()) {
      this.spinner.hide();
      return;
    }

    this.generalServices.Save(this.generalModules).subscribe(res => {
      if (res.Success) {
        this.pnotifyService.showSuccess('The Module have been saved correctly');
      }
      else {
        this.pnotifyService.showError(res.Messages.Message);
      }
    });

    this.spinner.hide();
    this.router.navigateByUrl('/module');
  }

  back() {
    this.router.navigateByUrl("/module");
  }

  addNewModule() {
    //Set inicial values on generalModule Header
    this.generalModules.GID = Guid.create().toString();
    this.generalModules.Indx = 0;
    this.generalModules.IsDeleted = false;
    this.generalModules.IsActive = true;
    this.generalModules.Status = Guid.createEmpty().toString();
    this.generalModules.IsMain = true;

  }

  addNewOption(template: TemplateRef<any>) {
    this.addOption = true;
    $("#option").modal();

    //Create a new option to add the array
    this.options = new Options();

    this.options.GID = Guid.create().toString();
    this.options.Indx = -1;
    this.options.Owner = this.generalModules.GID;
    this.options.IsSubM = false;

    //Set inicial values on Options Details
    this.generalModules.Options.push(this.options);
  }

  addNewSubOption(opt: any) {
    $("#subOption").modal();

    this.subOption = new Options();

    this.subOption.GID = Guid.create().toString();
    this.subOption.Indx = -1;
    this.subOption.Owner = opt.GID;

    opt.Options.push(this.subOption);

  }

  saveNewOption(opt: any) {
    if (this.ValidarOptionsValues()) { return; }

    if (this.options.IsSubM == false && (this.options.Options == undefined || this.options.Options.length > 0)) {
      this.options.Options = new Array<Options>()
    }

    this.generalModules.Options.forEach(x => { if (x.Indx == -1) { x.Indx = 0; } });

    $("#option").modal('hide');

  }

  saveNewSubOption(subOpt: any) {
    if (this.ValidarSubOptionsValues()) { return; }

    this.options.Options.forEach(x => { if (x.Indx == -1) { x.Indx = 0; } });

    $("#subOption").modal('hide');

  }

  editOption(opt: any) {
    //Create clone before edit
    this.optionClone = Object.assign({}, opt);

    $("#option").modal();

  }

  editSubOption(subOpt: any) {
    this.subOptionClone = Object.assign({}, subOpt);

    $("#subOption").modal();
  }

  cancelEditOption(opt: any) {
    let index = this.generalModules.Options.indexOf(opt);

    if (opt.Indx === -1) {
      this.generalModules.Options.splice(this.generalModules.Options.indexOf(opt), 1);
    }
    else {
      this.generalModules.Options[index] = this.optionClone;
    }

    $("#option").modal('hide');

  }

  cancelEditSubOption(subOpt: any) {

    let opt = this.generalModules.Options.find(x => x.GID == subOpt.Owner);
    let index = opt.Options.findIndex(x => x.GID == subOpt.GID);

    if (subOpt.Indx === -1) {
      this.options.Options.splice(opt.Options.indexOf(subOpt), 1);
    }
    else {
      this.options.Options[index] = this.subOptionClone;
    }

    $("#subOption").modal('hide');

  }

  removeOptions(opt: any) {
    if (opt <= 0) {
      opt.IsSubM = false;
      this.generalModules.Options.splice(this.generalModules.Options.indexOf(opt), 1);

      if ((this.generalModules.Options.length == undefined || this.generalModules.Options.length == 0)) {
        this.addOption = false;
      }
    }
    else {
      opt.IsActive = !opt.IsActive;
      opt.IsDeleted = !opt.IsDeleted;

      opt.Options.forEach(x => {
        x.IsActive = !x.IsActive;
        x.IsDeleted = !x.IsDeleted;
      });
    }
  }

  removeSubOptions(subOpt: any) {
    if (subOpt.Indx <= 0) {
      this.options.Options.splice(this.options.Options.indexOf(subOpt), 1);
    }
    else {
      subOpt.IsActive = !subOpt.IsActive;
      subOpt.IsDeleted = !subOpt.IsDeleted;
    }
  }

  isSubModule(opt: any) {
    opt.Link = "-";
  }

  selectOption(opt: any) {
    this.options = opt;
  }

  ValidarGeneralModule() {
    let hayErrores = false;
    /*console.log(this.translate.langs);
    let test = this.translate.getTranslation(this.translate.getDefaultLang())
    console.log(test);*/
    this.generalServices.ValidateModuleName(this.generalModules.Name).subscribe(res => {
      if (res.Success && this.generalModules.Indx <= 0) {
        this.pnotifyService.showError('Already exists a module with the same name');
        hayErrores = true;
      }
    });

    if (this.generalModules.Name == undefined || this.generalModules.Name == "") {
      this.pnotifyService.showError('The field Name cant be empty');
      hayErrores = true;
    }

    if (this.generalModules.Options.length == 0) {
      this.pnotifyService.showError('The module need to have at least 1 option added');
      hayErrores = true;
    }

    return hayErrores;
  }

  ValidarOptionsValues() {
    let hayErrores = false;

    this.generalServices.ValidateOptionName(this.options.Name, this.options.Owner).subscribe(res => {
      if (res.Success && this.options.Indx <= 0) {
        this.pnotifyService.showError('Already exists a option with the same name, for the module ' + this.generalModules.Name);
        hayErrores = true;
      }
    });

    if (this.options.Name == undefined || this.options.Name == "") {
      this.pnotifyService.showError('The field Name cant be empty');
      hayErrores = true;
    }

    if (!this.options.IsSubM && (this.options.Link == undefined || this.options.Link == "")) {
      this.pnotifyService.showError('The field Link cant be empty');
      hayErrores = true;
    }

    return hayErrores;
  }

  ValidarSubOptionsValues() {
    let hayErrores = false;

    this.generalServices.ValidateOptionName(this.subOption.Name, this.subOption.Owner).subscribe(res => {
      if (res.Success && this.subOption.Indx <= 0) {
        this.pnotifyService.showError('Already exists a option with the same name, for the module ' + this.options.Name);
        hayErrores = true;
      }
    });

    if (this.subOption.Name == undefined || this.subOption.Name == "") {
      this.pnotifyService.showError('The field Name cant be empty');
      hayErrores = true;
    }

    if (this.subOption.Link == undefined || this.subOption.Link == "") {
      this.pnotifyService.showError('The field Link cant be empty');
      hayErrores = true;
    }

    return hayErrores;
  }

}
