import { Component, TemplateRef, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Guid } from "guid-typescript";
import { TranslateService } from '@ngx-translate/core';
import { PnotifyService } from '../../../../services/pnotify.service';
import { UserService } from '../../../../services/user.service'
import { throwStatement } from 'babel-types';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {
  users: Array<User> = new Array<User>();

  constructor(private userServices: UserService, private router: Router, private activatedRoute: ActivatedRoute, private pnotifyService: PnotifyService, private spinner: NgxSpinnerService
    , private translate: TranslateService) {
    translate.use('user-es');
  }

  ngOnInit() {
    this.spinner.show();

    this.userServices.GetAll(true).subscribe(res => {
      if (res.Success) {
        this.users = res.Data;
      }
    });

    this.spinner.hide();
  }

  edit(sm: User) {
    this.router.navigateByUrl('/user/edit/' + sm.GID)
  }

  remove(us: User) {
    us.IsDeleted = true;
    us.IsActive = false;

    this.userServices.Save(us).subscribe(res => {
      if (!res.Success) {
        this.pnotifyService.showError("The user can't be remove");
      }
      else {
        this.pnotifyService.showSuccess("The user has remove correctly");
        this.ngOnInit();

      }
    });

  }

}
