import { Component, TemplateRef, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Guid } from "guid-typescript";
import { TranslateService } from '@ngx-translate/core';
import { PnotifyService } from '../../../services/pnotify.service';
import { UserService } from '../../../services/user.service';
import { RolesService } from '../../../services/roles.service';
import { PartyService } from 'src/app/services/party.service';
import { GeneralTypeService } from 'src/app/services/general-type.service';
import { throwStatement } from 'babel-types';
import { User } from 'src/app/models/user';
import { _appIdRandomProviderFactory } from '@angular/core/src/application_tokens';
import { Parties } from 'src/app/models/parties';
import { UserRol } from 'src/app/models/Roles';
import { GeneralTypes } from 'src/app/models/general-types';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  user: User = new User();
  person: Parties = new Parties();
  confPassId: string;
  roles: Array<UserRol> = new Array<UserRol>();
  genders: Array<GeneralTypes> = new Array<GeneralTypes>();
  departments: Array<GeneralTypes> = new Array<GeneralTypes>();
  positions: Array<GeneralTypes> = new Array<GeneralTypes>();

  constructor(private userServices: UserService, private roleServices: RolesService, private router: Router
    , private activatedRoute: ActivatedRoute, private pnotifyService: PnotifyService, private partyServices: PartyService
    , private spinner: NgxSpinnerService, private translate: TranslateService, private generalServices: GeneralTypeService) {
    translate.use('user-es');
  }

  ngOnInit() {
    this.loadBasicInfo();

    let id = this.activatedRoute.snapshot.paramMap.get('id');

    if (id) {
      this.getUserById(id);
    }
    else {
      this.addNewUser();
    }
  }

  validarUsuario() {
    let error = false;

    this.userServices.ValidateUserName(this.user.UsrId).subscribe(res => {
      if (res.Data) {
        this.pnotifyService.showError("This user name already exists.");
        error = true;
      }
    });


    if (this.user.UsrId == undefined || this.user.UsrId == "") {
      this.pnotifyService.showError("The user name can't be empty");
      error = true;
    }

    if (this.user.Indx == 0 || this.user.Indx == undefined) {
      if (this.user.PssId == undefined || this.user.PssId != this.confPassId) {
        this.pnotifyService.showError("The user password is empty or not mach");
        error = true;
      }
    }

    if (this.user.UserRoleGID == "00000000-0000-0000-0000-000000000000") {
      this.pnotifyService.showError("You must be select a role");
    }

    if (this.user.Department == undefined) {
      this.pnotifyService.showError("You meed select a department");
    }

    if (this.user.Position == undefined) {
      this.pnotifyService.showError("You need select a position");
    }


    if (this.user.Party.FirstName == undefined) {
      this.pnotifyService.showError("The field name can't be empty");
    }

    if (this.user.Party.LastName == undefined) {
      this.pnotifyService.showError("The field last name can't be empty");
    }

    if (this.user.Party.Identifications == undefined || this.user.Party.Identifications.length == 0) {
      this.pnotifyService.showError("You need add at least 1 identification");
    }

    return error;
  }

  loadBasicInfo() {
    //Load Role list for Rol DropDown
    this.roleServices.GetUserRoles().subscribe(res => {
      if (res.Success) {
        this.roles = res.Data;
      }
    });

    //Load Gender list for party gender DropDown
    this.partyServices.GetGenderTypes().subscribe(res => {
      if (res.Success) {
        this.genders = res.Data;
      }
    });

    //get departaments for dropdown Parent code(DEP)
    this.generalServices.GetDetailsByParentCode("DEP").subscribe(res => {
      if (res.Success) {
        this.departments = res.Data;
      }
    });

  }

  getUserById(id: string) {
    this.userServices.GetUserById(id).subscribe(res => {
      console.log(res.Data);
      if (!res.Success) {
        this.pnotifyService.showError("A error has occurred");
      }
      else{
        this.user = res.Data;
      }
    });
  }

  addNewUser() {
    this.user.GID = Guid.create().toString();
    this.user.UserRoleGID = Guid.createEmpty().toString();

    //Set Party GID
    this.user.Party.GID = Guid.create().toString();
    this.user.PartyGID = this.user.Party.GID
  }

  save() {
    if (this.validarUsuario()) { return; }
    this.spinner.show();
    this.userServices.Save(this.user).subscribe(res => {
      if (res.Success) {
        this.router.navigateByUrl('/users');
      }
      else {
        this.pnotifyService.showError("An Error has occurred ed trying to create user");
      }
      this.spinner.hide();
    });
  }

  back() {
    this.router.navigateByUrl('/users');
  }

  onChangeDep(obj: any) {
    this.generalServices.GetChieldsByParenId(obj).subscribe(res => {
      if (res.Success) {
        this.positions = res.Data;
      }
    });
    this.user.Position = undefined;
  }

}
