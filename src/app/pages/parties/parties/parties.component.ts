import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Parties } from 'src/app/models/parties';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Guid } from 'guid-typescript';
import { GeneralTypes } from 'src/app/models/general-types';
import { GeneralTypeService } from 'src/app/services/general-type.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-parties',
  templateUrl: './parties.component.html',
  styleUrls: ['./parties.component.css']
})
export class PartiesComponent implements OnInit {

  @Input() person: Parties = new Parties();
  @Output() personFormStatus: EventEmitter<any> = new EventEmitter<any>();
  personForm: FormGroup;
  contactNumberFormStatus = false;
  genders: Array<GeneralTypes> = new Array<GeneralTypes>();

  constructor(private translateService: TranslateService, formBuilder: FormBuilder, private generalService: GeneralTypeService) {
    this.translateService.use('parties-es');
    this.personForm = formBuilder.group({
      name: ['', Validators.required],
      lastname: ['', Validators.required],
      gender: [''],
      birthdate: ['', Validators.required],
      email: ['', Validators.email]
    });
  }

  ngOnInit() {
    this.personForm.statusChanges.subscribe(data => {
      const status = data === 'VALID' ? true : false;
      // status = (status && this.person.ContactNumbers.length > 0);
      this.personFormStatus.emit(status);
    });

    this.getGendersTypes();
    this.person.GID = Guid.create.toString();
  }

  getGendersTypes() {
    this.generalService.GetGeneralTypeByCode(environment.generalTypesCode.PartyGender).subscribe(result => {
      if (result.Success) {
        this.genders = result.Data.Chields;

      }
    });
  }

}
