import { PnotifyService } from './../../services/pnotify.service';
import { AuthenticationService } from './../../services/authentication.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [AuthenticationService]
})
export class LoginComponent implements OnInit {

  userName: string;
  password: string;
  returnUrl: string;
  pnotify = undefined;

  constructor(private authService: AuthenticationService, private activatedRoute: ActivatedRoute,
    private spinner: NgxSpinnerService, pnotifyService: PnotifyService, private translate: TranslateService) {
      translate.setDefaultLang('login-es');

      this.pnotify = pnotifyService.getPNotify();

      this.returnUrl = this.activatedRoute.snapshot.queryParams['returnUrl'] || '/dashboard';

      if (localStorage.getItem(environment.keyLoginLocalStorage)) {
        window.location.href = this.returnUrl;
      }
    }

  ngOnInit() {
    // this.pnotify.notice({
    //   text: 'Im a notice.'
    // });
    // this.pnotify.info({
    //   text: 'Im an info message.'
    // });
    // this.pnotify.success({
    //   text: 'Im a success message.'
    // });
    // this.pnotify.error({
    //   text: 'Im an error message.'
    // });
  }

  login() {
    this.spinner.show();
    this.authService.login(this.userName, this.password).subscribe(result => {
      if (result.Success) {
        localStorage.setItem(environment.keyLoginLocalStorage, JSON.stringify(result.Data));
        window.location.href = this.returnUrl;
      } else {
        this.pnotify.error(result.Messages.Message);
      }

      this.spinner.hide();
    });
  }

  keyPress(ev) {
    if (ev.keyCode === 13) {
      this.login();
    }
  }

}
