import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Vehicles } from 'src/app/models/vehicles';
import { TranslateService } from '@ngx-translate/core';
import { GeneralTypeService } from 'src/app/services/general-type.service';
import { GeneralTypes } from 'src/app/models/general-types';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Guid } from 'guid-typescript';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.css']
})
export class VehiclesComponent implements OnInit {

  @Input() vehicle: Vehicles = new Vehicles();
  @Output() vehicleFormStatus: EventEmitter<any> = new EventEmitter<any>();
  vehicleForm: FormGroup;
  brandTypes: GeneralTypes[];
  constructor(private translateService: TranslateService, private generalService: GeneralTypeService, formBuilder: FormBuilder) {
    // translateService.use('vehicle-es');
    this.vehicleForm = formBuilder.group({
      brand: ['', Validators.required],
      model: ['', Validators.required],
      color: ['', Validators.required],
      year: ['', Validators.required],
      licensePlate: ['', Validators.required],
      vin: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.GetAllBrand();
    this.vehicleForm.statusChanges.subscribe(result => {
      const status = result === 'VALID' ? true : false;
      this.vehicleFormStatus.emit(status);
    });

    this.vehicle.GID = Guid.create().toString();
  }

  GetAllBrand() {
    this.generalService.GetBrandTypes().subscribe(result => {
      if (result.Success) {
        this.brandTypes = result.Data;
      }
    });
  }

  GetAllModels() {
    this.generalService.GetGeneralTypeByCode(environment.generalTypesCode.Brand).subscribe(result => {
      console.log(result);
    });
  }
}
