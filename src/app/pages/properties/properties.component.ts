import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Properties } from 'src/app/models/properties';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Guid } from 'guid-typescript';

@Component({
  selector: 'app-properties',
  templateUrl: './properties.component.html',
  styleUrls: ['./properties.component.css']
})
export class PropertiesComponent implements OnInit {

  @Input() property: Properties = new Properties();
  @Output() propertyFormStatus: EventEmitter<any> = new EventEmitter<any>();
  propertyForm: FormGroup;

  constructor(private translateService: TranslateService, formBuilder: FormBuilder) {
    // this.translateService.use('properties-es');
    this.propertyForm = formBuilder.group({
      type: ['', Validators.required],
      levels: ['', Validators.required],
      meters: ['', Validators.required],
      address: ['', Validators.required],
      haspool: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.propertyForm.statusChanges.subscribe(result => {
      const status = result === 'VALID' ? true : false;
      this.propertyFormStatus.emit(status);
    });
    this.property.GID = Guid.create().toString();
  }

}
