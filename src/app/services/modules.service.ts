import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { BaseResponse } from '../models/base-response';
import { GeneralModules } from '../models/GeneralModule';

@Injectable({
  providedIn: 'root'
})
export class ModulesService {

  constructor(private http: HttpClient) { }

  GetMainModule() {
    return this.http.get<BaseResponse<GeneralModules[]>>(`${environment.baseUrlApi}Modules/GetAllMainModules`);
  }

  /*
  Save(request: any) {
    return this.http.post<BaseResponse<LoansRequest>>(`${environment.baseUrlApi}LoanRequest/CreateRequest`, request);
  }*/
}
