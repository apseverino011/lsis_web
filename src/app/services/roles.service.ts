import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { RolesModulesOptions, UserRol } from '../models/Roles';
import { BaseResponse } from './../models/base-response';
import { GeneralModules } from '../models/GeneralModule';

@Injectable({
  providedIn: 'root'
})
export class RolesService {

  constructor(private http: HttpClient) { }

  GetUserRoles(): any {
    return this.http.get<BaseResponse<Array<UserRol>>>(`${environment.baseUrlApi}UserRoles/FindBy?isConstruct=true`);
  }

  GetModulosByCompany(): any {
    return this.http.get<BaseResponse<GeneralModules>>(`${environment.baseUrlApi}Modules/GetAllModuleCompany`);
  }

  GetUserRoleToEdit(gid: string){
    return this.http.get<BaseResponse<UserRol>>(`${environment.baseUrlApi}UserRoles/GetRoleToEdit?roleId=${gid}`);
  }

  Save(userRole: UserRol): any {
    const model = userRole;
    return this.http.post<BaseResponse<GeneralModules>>(`${environment.baseUrlApi}roles/SaveRol`,model);
  }
}
