import { TestBed } from '@angular/core/testing';

import { GeneralTypeService } from './general-type.service';

describe('GeneralTypeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GeneralTypeService = TestBed.get(GeneralTypeService);
    expect(service).toBeTruthy();
  });
});
