import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { BaseResponse } from '../models/base-response';
import { LoansRequest } from '../models/loans-request';

@Injectable({
  providedIn: 'root'
})
export class LoansRequestService {

  constructor(private http: HttpClient) { }

  GetAll() {
    // return this.http.get<BaseResponse<LoansRequest>>(`${environment.baseUrlApi}LoanRequest/GetAll`);
    return this.http.get<BaseResponse<LoansRequest[]>>(`${environment.baseUrlApi}LoanRequest/GetAll`);
  }

  FindByNumber(number: string) {
    return this.http.get<BaseResponse<LoansRequest>>(`${environment.baseUrlApi}LoanRequest/FindByNumber?number=${number}`);
  }

  CreateRequest(request: any) {
    return this.http.post<BaseResponse<LoansRequest>>(`${environment.baseUrlApi}LoanRequest/CreateRequest`, request);
  }

  test() {
    return true;
  }
}
