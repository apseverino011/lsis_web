import { Injectable } from '@angular/core';
import { BaseResponse } from './../models/base-response';
import { GeneralModules } from './../models/GeneralModule';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class GeneralModulesService {

  constructor(private http: HttpClient) { }

  ValidateOptionName(name: string, owner: string): any {
    return this.http.get<BaseResponse<GeneralModules>>(`${environment.baseUrlApi}Modules/ValidateOptionName?name=${name}&owner=${owner}`);
  }

  ValidateModuleName(name: string): any {
    return this.http.get<BaseResponse<GeneralModules>>(`${environment.baseUrlApi}Modules/ValidateModuleName?name=${name}`);
  }

  GetByGID(gid: string): any {
    return this.http.get<BaseResponse<GeneralModules>>(`${environment.baseUrlApi}Modules/GetByGID?gid=${gid}&isContruct=true`);

  }

  GetAllMainModules(isConstruct: boolean): any {
    return this.http.get<BaseResponse<Array<GeneralModules>>>(`${environment.baseUrlApi}Modules/GetAllMainModules?isConstruct=${isConstruct}`);

  }

  GetAllSubModules(isConstruct: boolean): any {
    return this.http.get<BaseResponse<Array<GeneralModules>>>(`${environment.baseUrlApi}Modules/GetAllSubModules?isConstruct=${isConstruct}`);

  }

  GetAllOptions(isConstruct: boolean): any {
    return this.http.get<BaseResponse<Array<GeneralModules>>>(`${environment.baseUrlApi}Modules/GetAllOptions?isConstruct=${isConstruct}`);
  }

  Save(genModule: GeneralModules): any {
    const model = genModule;
    return this.http.post<BaseResponse<GeneralModules>>(`${environment.baseUrlApi}Modules/CreateModule`, model);
  }

}
