import { TestBed } from '@angular/core/testing';

import { AccountPeriodsService } from './account-periods.service';

describe('AccountPeriodsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AccountPeriodsService = TestBed.get(AccountPeriodsService);
    expect(service).toBeTruthy();
  });
});
