import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseResponse } from '../models/base-response';
import { Company } from 'src/app/models/Company';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(private http: HttpClient) { }

  GetAll(): any {
    return this.http.get<BaseResponse<Company[]>>(`${environment.baseUrlApi}Company/GetAll`);
  }

  GetCompanyById(gid: string): any {
    return this.http.get<BaseResponse<Company>>(`${environment.baseUrlApi}Company/FindCompanyById?companyId=${gid}&isConstruct=true`);
  }

  Save(company: Company) {
    return this.http.post<BaseResponse<Company>>(`${environment.baseUrlApi}Company/Save`, company);
  }

}
