import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AccountClass } from 'src/app/models/AccClass';
import { BaseResponse } from '../models/base-response';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class AccountClassService {

  constructor(private http: HttpClient) { }

  GetAll(isConstruct: boolean = false) {
    return this.http.get<any>(`${environment.baseUrlApi}class/GetAll?isConstruct=${isConstruct}`);
  }

  FindById(gid: string, isConstruct: boolean = false) {
    return this.http.get<BaseResponse<AccountClass>>(`${environment.baseUrlApi}class/FindById?gid=${gid}&isConstruct=${isConstruct}`);
  }

  FindClassesByGroupId(gid: string, isConstruct: boolean = false) {
    return this.http.get<BaseResponse<AccountClass[]>>(`${environment.baseUrlApi}class/FindClassesByGroupId?groupId=${gid}&isConstruct=${isConstruct}`);
  }

  Save(_class: AccountClass) {
    return this.http.post<BaseResponse<AccountClass[]>>(`${environment.baseUrlApi}class/Save`, _class);
  }
}
