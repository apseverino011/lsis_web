import { Injectable } from '@angular/core';
import { BaseResponse } from './../models/base-response';
import { User } from './../models/user';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { GeneralModules } from '../models/GeneralModule';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  GetAll(isConstruct: boolean = false): any {
    return this.http.get<BaseResponse<Array<User>>>(`${environment.baseUrlApi}User/GetAll?isConstruct=${isConstruct}`);
  }

  GetUserById(gid: string): any {
    return this.http.get<BaseResponse<User>>(`${environment.baseUrlApi}User/GetUserById?gid=${gid}&isConstruct=true`);
  }

  ValidateUserName(userName: string): any {
    return this.http.get<BaseResponse<Array<User>>>(`${environment.baseUrlApi}User/ValidateUserName?userName=${userName}`);
  }

  Save(user: User): any {
    const model = user;
    return this.http.post<BaseResponse<User>>(`${environment.baseUrlApi}User/Save`, model);
  }
}
