import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AccountGroup } from './../models/AccountGroup';
import { BaseResponse } from '../models/base-response';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AccountGroupService {

  constructor(private http: HttpClient) { }

  GetNextAccNumber(accGroupId: string) {
    return this.http.get<BaseResponse<string>>(`${environment.baseUrlApi}group/GetNextAccNumber?groupId=${accGroupId}`);
  }

  GetNextSubAccNumber(accGroupId: string) {
    return this.http.get<BaseResponse<string>>(`${environment.baseUrlApi}group/GetNextSubAccNumber?groupId=${accGroupId}`);
  }

  SetNextSubAccNumber(accGroupId: string) {
    return this.http.put<any>(`${environment.baseUrlApi}group/SetNextSubAccNumber/${accGroupId}`, null);
  }

  GetAll(isConstruct: boolean) {
    return this.http.get<BaseResponse<AccountGroup[]>>(`${environment.baseUrlApi}group/GetAll?isConstruct=${isConstruct}`);
  }

  FindById(gid: string, isConstruct: boolean) {
    return this.http.get<BaseResponse<AccountGroup>>(`${environment.baseUrlApi}group/FindById?gid=${gid}&isConstruct=${isConstruct}`);
  }

  Save(group: AccountGroup) {
    return this.http.post<BaseResponse<AccountGroup[]>>(`${environment.baseUrlApi}group/Save`, group);
  }

}
