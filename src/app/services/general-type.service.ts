
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseResponse } from '../models/base-response';
import { GeneralTypes } from '../models/general-types';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GeneralTypeService {

  constructor(private http: HttpClient) { }

  validateCode(code: string): any {
    return this.http.get<BaseResponse<boolean>>(`${environment.baseUrlApi}GeneralType/ValidateCode?code=${code}`);
  }
  
  Save(generalType: GeneralTypes) {
    const model = generalType;
    return this.http.post<BaseResponse<GeneralTypes>>(`${environment.baseUrlApi}GeneralType/Save`,model);
  }

  GetChieldsByParenId(gid: string): any {
    return this.http.get<BaseResponse<GeneralTypes>>(`${environment.baseUrlApi}GeneralType/FindChieldsByParentGid?parentId=${gid}&isConstruct=true`);
  }

  GetGeneralTypeByCode(code: string): any {
    return this.http.get<BaseResponse<GeneralTypes>>(`${environment.baseUrlApi}GeneralType/FindByCode?code=${code}&isConstruct=true`);
  }

  GetDetailsByParentCode(code: string): any {
    return this.http.get<BaseResponse<Array<GeneralTypes>>>(`${environment.baseUrlApi}GeneralType/FindChieldsByParentCode?parentCode=${code}`);
  }

  FindByGID(gid: string, isConstruct: boolean) {
    return this.http.get<BaseResponse<GeneralTypes>>(`${environment.baseUrlApi}GeneralType/FindByGid?gtypeId=${gid}&isConstruct=${isConstruct}`);
  }

  GetAllParents(isConstruct: boolean) {
    return this.http.get<BaseResponse<GeneralTypes[]>>(`${environment.baseUrlApi}GeneralType/GetAllParent?isConstruct=${isConstruct}`);
  }

  GetIdentificationTypes() {
    return this.http.get<BaseResponse<GeneralTypes[]>>(`${environment.baseUrlApi}GeneralType/FindChieldsByParentCode?`
    + `parentCode=${environment.generalTypesCode.PartyIdentification}`);
  }

  GetPartyContactTypes() {
    return this.http.get<BaseResponse<GeneralTypes[]>>(`${environment.baseUrlApi}GeneralType/FindChieldsByParentCode?`
    + `parentCode=${environment.generalTypesCode.PartyContactTypes}`);
  }

  GetBrandTypes() {
    return this.http.get<BaseResponse<GeneralTypes[]>>(`${environment.baseUrlApi}GeneralType/FindChieldsByParentCode?`
    + `parentCode=${environment.generalTypesCode.Brand}`);
  }

  GetWarrantyTypes() {
    return this.http.get<BaseResponse<GeneralTypes[]>>(`${environment.baseUrlApi}GeneralType/FindChieldsByParentCode?`
    + `parentCode=${environment.generalTypesCode.WarrantyType}`);
  }

  GetRequestTypes() {
    return this.http.get<BaseResponse<GeneralTypes[]>>(`${environment.baseUrlApi}GeneralType/FindChieldsByParentCode?`
    + `parentCode=${environment.generalTypesCode.RequestType}`);
  }

  GetTermTypes() {
    return this.http.get<BaseResponse<GeneralTypes[]>>(`${environment.baseUrlApi}GeneralType/FindChieldsByParentCode?`
    + `parentCode=${environment.generalTypesCode.TermType}`);
  }
}
