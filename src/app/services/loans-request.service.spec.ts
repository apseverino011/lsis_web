import { TestBed } from '@angular/core/testing';

import { LoansRequestService } from './loans-request.service';

describe('LoansRequestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoansRequestService = TestBed.get(LoansRequestService);
    expect(service).toBeTruthy();
  });
});
