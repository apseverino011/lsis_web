import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { GeneralTypes } from 'src/app/models/general-types';
import { BaseResponse } from './../models/base-response';
import { Parties } from 'src/app/models/parties';

@Injectable({
  providedIn: 'root'
})

export class PartyService {

  constructor(private http: HttpClient) { }

  GetGenderTypes(): any {
    return this.http.get<BaseResponse<Array<GeneralTypes>>>(`${environment.baseUrlApi}GeneralType/FindChieldsByParentCode?parentCode=PTG`);
  }

  FindPartyByGID(gid: string): any {
    return this.http.get<BaseResponse<Array<Parties>>>(`${environment.baseUrlApi}Party/Find?gid=${gid}&isConstruct=true`);
  }

  FindPartyByName(name: string): any {
    return this.http.get<BaseResponse<Array<Parties>>>(`${environment.baseUrlApi}Party/Find?name=${name}`);
  }
  
  FindPartyByIdent(identNumber: string): any {
    return this.http.get<BaseResponse<Array<Parties>>>(`${environment.baseUrlApi}Party/FindByIdent?identNumber=${identNumber}`);
  }

}
