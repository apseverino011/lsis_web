import { TestBed } from '@angular/core/testing';

import { AccountClassService } from './account-class.service';

describe('AccountClassService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AccountClassService = TestBed.get(AccountClassService);
    expect(service).toBeTruthy();
  });
});
