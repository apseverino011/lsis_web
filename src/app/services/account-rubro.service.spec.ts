import { TestBed } from '@angular/core/testing';

import { AccountRubroService } from './account-rubro.service';

describe('AccountRubroService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AccountRubroService = TestBed.get(AccountRubroService);
    expect(service).toBeTruthy();
  });
});
