import { BaseResponse } from './../models/base-response';
import { Login } from './../models/login';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient) { }

    login(username: string, password: string): any {
        // return this.http.post<any>(`http://localhost:4200/users/authenticate`, { username, password })
        const model = {UserName: username, Password: password};
        return this.http.post<BaseResponse<Login>>(`${environment.baseUrlApi}Log/In`, model);
    }

    logout() {
        // remove user from local storage to log user out
        this.http.post<any>(`${environment.baseUrlApi}Log/Out`, {}).subscribe();
        localStorage.removeItem(environment.keyLoginLocalStorage);
        window.location.reload();
    }
}
