import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { GeneralTypes } from 'src/app/models/general-types';
import { BaseResponse } from './../models/base-response';
import { PeriodState } from 'src/app/models/PeriodState';

@Injectable({
  providedIn: 'root'
})

export class PeriodStateService {

  constructor(private http: HttpClient) { }

  GetAll(): any {
    return this.http.get<BaseResponse<Array<PeriodState>>>(`${environment.baseUrlApi}periodstate/GetAll`);
  }
}
