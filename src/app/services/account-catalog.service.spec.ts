import { TestBed } from '@angular/core/testing';

import { AccountCatalogService } from './account-catalog.service';

describe('AccountCatalogService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AccountCatalogService = TestBed.get(AccountCatalogService);
    expect(service).toBeTruthy();
  });
});
