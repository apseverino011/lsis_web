import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseResponse } from '../models/base-response';
import { AccountPeriod } from 'src/app/models/AccountPeriod';
import { environment } from 'src/environments/environment';
import { PeriodDto } from '../models/PeriodDto';

@Injectable({
  providedIn: 'root'
})
export class AccountPeriodsService {

  constructor(private http: HttpClient) { }

  GetPeriods(): any {
    return this.http.get<PeriodDto[]>(`${environment.baseUrlApi}period/GetPeriods`);
  }

  GetAll(): any {
    return this.http.get<BaseResponse<AccountPeriod[]>>(`${environment.baseUrlApi}accountPeriod/GetAll`);
  }

  Find(gid: string): any {
    return this.http.get<BaseResponse<AccountPeriod>>(`${environment.baseUrlApi}accountPeriod/Find?gid=${gid}&isConstruct=true`);
  }

  LockPeriod(periodId: string){
    return this.http.put<any>(`${environment.baseUrlApi}accountPeriod/LockPeriod/${periodId}`,null); 
  }

  ClosePeriod(periodId: string){
    return this.http.put<any>(`${environment.baseUrlApi}accountPeriod/ClosePeriod/${periodId}`,null);

  }

  LockPeriodDetail(periodDetailId: string){
    return this.http.put<any>(`${environment.baseUrlApi}accountPeriod/LockPeriodDetail/${periodDetailId}`,null);

  }

  ClosePeriodDetail(periodDetailId: string){
    return this.http.put<any>(`${environment.baseUrlApi}accountPeriod/ClosePeriodDetail/${periodDetailId}`,null);

  }

  Save(period: AccountPeriod) {
    return this.http.post<BaseResponse<AccountPeriod>>(`${environment.baseUrlApi}accountPeriod/Save`, period);
  }

}
