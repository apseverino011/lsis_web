import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { BaseResponse } from 'src/app/models/base-response';
import { AccountRubro } from 'src/app/models/AccountRubro';

@Injectable({
  providedIn: 'root'
})
export class AccountRubroService {

  constructor(private http: HttpClient) { }

  GetAll(isConstruct: boolean = false) {
    return this.http.get<BaseResponse<AccountRubro[]>>(`${environment.baseUrlApi}rubro/GetAll?isConstruct=${isConstruct}`);
  }
  
  FindById(gid: string, isConstruct: boolean = false) {
    return this.http.get<BaseResponse<AccountRubro>>(`${environment.baseUrlApi}rubro/FindById?gid=${gid}&isConstruct=${isConstruct}`);
  }
  
  FindRubrosByGroupId(gid: string, isConstruct: boolean = false) {
    return this.http.get<BaseResponse<AccountRubro[]>>(`${environment.baseUrlApi}rubro/FindRubrosByGroupId?groupId=${gid}&isConstruct=${isConstruct}`);
  }
  
  Save(rubro: AccountRubro) {
    return this.http.post<BaseResponse<AccountRubro[]>>(`${environment.baseUrlApi}rubro/Save`,rubro);
  }

}
