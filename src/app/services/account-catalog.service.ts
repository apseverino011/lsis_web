import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AccountCatalog } from 'src/app/models/AccCatalog';
import { BaseResponse } from 'src/app/models/base-response';
import { environment } from 'src/environments/environment';
import { ParentAccount } from '../models/ParentAccount';

@Injectable({
  providedIn: 'root'
})
export class AccountCatalogService {

  constructor(private http: HttpClient) { }

  GetAll(isConstruct: boolean) {
    return this.http.get<BaseResponse<AccountCatalog[]>>(`${environment.baseUrlApi}account/GetAll?isConstruct=${isConstruct}`);
  }

  GetParents(){
    return this.http.get<BaseResponse<ParentAccount[]>>(`${environment.baseUrlApi}account/GetParents`);
  }

  FindById(gid: string, isConstruct: boolean) {
    return this.http.get<BaseResponse<AccountCatalog>>(`${environment.baseUrlApi}account/FindById?gid=${gid}&isConstruct=${isConstruct}`);
  }

  Save(catalog: AccountCatalog) {
    return this.http.post<BaseResponse<AccountCatalog[]>>(`${environment.baseUrlApi}account/Save`, catalog);
  }
}
