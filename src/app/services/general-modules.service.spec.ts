import { TestBed } from '@angular/core/testing';

import { GeneralModulesService } from './general-modules.service';

describe('GeneralModulesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GeneralModulesService = TestBed.get(GeneralModulesService);
    expect(service).toBeTruthy();
  });
});
