import { TestBed } from '@angular/core/testing';

import { PeriodStateService } from './period-state.service';

describe('PeriodStateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PeriodStateService = TestBed.get(PeriodStateService);
    expect(service).toBeTruthy();
  });
});
